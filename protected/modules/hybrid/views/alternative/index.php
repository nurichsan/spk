<?php
/* @var $this AlternativeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ahp Alternatives',
);

$this->menu=array(
	array('label'=>'Create AhpAlternative', 'url'=>array('create')),
	array('label'=>'Manage AhpAlternative', 'url'=>array('admin')),
);
?>

<h1>Ahp Alternatives</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
