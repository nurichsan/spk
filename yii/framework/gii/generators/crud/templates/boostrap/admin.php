<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Manage',
);\n";
?>

$this->menu=array(
array('label'=>'List <?php echo $this->modelClass; ?>', 'url'=>array('index')),
array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#<?php echo $this->class2id($this->modelClass); ?>-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-list"></i>
            Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?>
        </div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <?php
            echo "<?php ";
            echo '$button = \'<button class="btn green"><i class="fa fa-plus"></i></button>\';';
            ?>
            <?php echo 'echo CHtml::link($button, Yii::app()->controller->createUrl("create"));?>'; ?>
            <?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'itemsCssClass' => 'table table-striped table-bordered',
            'columns'=>array(
            <?php
            $count = 0;
            foreach ($this->tableSchema->columns as $column) {
                if (++$count == 7)
                    echo "\t\t/*\n";
                echo "\t\t'" . $column->name . "',\n";
            }
            if ($count >= 7)
                echo "\t\t*/\n";
            ?>
            array(
            'class'=>'CButtonColumn',
            'template' => '{view} {update} {delete}',
            'buttons' => array(
            'delete' => array(
            'imageUrl' => Yii::app()->theme->baseUrl . '/images/delete.png'
            ),
            'update' => array(
            'imageUrl' => Yii::app()->theme->baseUrl . '/images/edit.png'
            ),
            'view' => array(
            'imageUrl' => Yii::app()->theme->baseUrl . '/images/view.png'
            )
            ),
            'htmlOptions' => array(
            'style' => 'width:75px;text-align:center;'
            )
            ),
            ),
            )); ?>
        </div>
    </div>
</div>