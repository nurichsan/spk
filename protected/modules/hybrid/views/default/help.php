<div class="span7">
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-bullhorn"></i>                    
            </div>
        </div>
        <div class="block-content collapse in">                            
            <div class="row-fluid">
                <h4>Petunjuk Penggunaan</h4>
                <p>
                <ul>
                    <li>Jika menambah kriteria atau <i>alternative</i> baru. Pastikan setiap <i>alternative</i> juga memiliki nilai terhadap kriteria.</li>                    
                </ul>
                </p>
            </div>
        </div>
    </div>
</div>