<?php
/* @var $this CriteriaController */
/* @var $model SawCriteria */

$this->breadcrumbs=array(
	'Saw Criterias'=>array('index'),
	$model->criteria_id,
);

$this->menu=array(
array('label'=>'List SawCriteria', 'url'=>array('index')),
array('label'=>'Create SawCriteria', 'url'=>array('create')),
array('label'=>'Update SawCriteria', 'url'=>array('update', 'id'=>$model->criteria_id)),
array('label'=>'Delete SawCriteria', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->criteria_id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SawCriteria', 'url'=>array('admin')),
);
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-search"></i>
            View SawCriteria #<?php echo $model->criteria_id; ?>        </div>
    </div>
    <div class="portlet-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'criteria_id',
		'criteria_name',
		'prosen',
        ),
        )); ?>
    </div>
</div>