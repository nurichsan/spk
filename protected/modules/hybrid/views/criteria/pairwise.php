<div class="span12">
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">Pairwise Comparison Table</span>
            </div>							
        </div>
        <div class="block-content collapse in">            
            <div class="form-body">
                <div style="color: blue; font-weight: blue; background-color: yellow; margin-bottom: 10px;" class="span3">
                    Product : Jumlah Perkalian Setiap Kolom <br />
                    Akar : Product pangkat (1/jumlah alternative) <br />
                    Bobot : Akar[i,j] / Total Akar
                </div>
                <?php
                foreach ($category as $cat) :
                    ?>
                    <table class="table table-bordered">
                        <tr>
                            <th>#</th>
                            <?php
                            $all_criteria = AhpCriteria::model()->findAllByAttributes(array('category_id' => $cat->category_id));
                            foreach ($all_criteria as $td) {
                                echo "<th>" . $td->label . "</th>";
                            }
                            echo "<th>Pruduct</th>";
                            echo "<th>Akar</th>";
                            echo "<th>Bobot</th>";
                            ?>
                        </tr>
                        <?php
                        $data = PairWiseCalculation::getData($all_criteria, $cat->category_id);
                        $i = 0;
                        $total_akar = 0;
                        $total_bobot = 0;
                        $bobot = array();
                        $sum = array();
                        for ($j = 1; $j < count($data[$i]); $j++) {
                            $sum[$j] = 0;
                        }
                        for ($i = 0; $i < count($data); $i++) {
                            echo "<tr>";
                            for ($j = 0; $j < count($data[$i]); $j++) {
                                if (($i + 1) == $j) {
                                    echo "<td><span style='color:red;font-weight:bold;'>" . $data[$i][$j] . "</span></td>";
                                } else {
                                    // return $data[i][j] bisa berupa label kriteria
                                    echo "<td>" . (is_numeric($data[$i][$j]) ? floatval($data[$i][$j]) : $data[$i][$j]) . "</td>";
                                }
                                if ($j != 0) {
                                    $sum[$j] += $data[$i][$j];
                                }
                            }
                            $total_akar += $data[$i][$j - 2]; //akar
                            $total_bobot += $data[$i][$j - 1]; //bobot                        
                            $bobot[$i] = $data[$i][$j - 1];
                            echo "</tr>";
                        }
                        echo "<tr style='color:blue;font-weight:bold;'>";
                        echo "<td align=right>Jumlah</td>";
                        for ($j = 1; $j <= count($sum); $j++) {
                            echo "<td>" . $sum[$j] . "</td>";
                        }
                        echo "</tr>";
                        ?>
                        <table class="table table-bordered">
                            <?php
                            echo "<tr>";
                            echo "<td>Jml * Bbt</td>";
                            $lamdaMax = 0;
                            for ($i = 0; $i < count($bobot); $i++) {
                                echo "<td>" . number_format(($sum[$i + 1] * $bobot[$i]), 3, '.', ',') . "</td>";
                                $lamdaMax += ($sum[$i + 1] * $bobot[$i]);
                            }
                            echo "</tr>";
                            $ci = ($lamdaMax - count($all_criteria)) / (count($all_criteria) - 1);
                            $index = AhpConsistencyIndex::model()->findByPk(count($all_criteria));
                            $ri = $index->value;
                            $cr = $ci / $ri;
                            ?>
                        </table>
                        <div>
                            <h4><?php echo 'Lamda Max : ' . number_format($lamdaMax, 3, '.', ','); ?></h4>
                            <h4><?php echo 'CI : ' . number_format($ci, 3, '.', '.'); ?></h4>
                            <h4><?php echo 'CR (CI/RI) : ' . number_format($cr, 3, '.', ',') . ' (' . ($cr < 0.1 ? 'Konsisten' : 'Tidak Konsisten') . ')'; ?></h4>
                        </div>
                    </table>
                <?php endforeach; ?>
            </div>
        </div>
    </div><!--form-->
</div>