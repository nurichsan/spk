<?php
/* @var $this CriteriaController */
/* @var $data SawCriteria */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('criteria_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->criteria_id), array('view', 'id'=>$data->criteria_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('criteria_name')); ?>:</b>
	<?php echo CHtml::encode($data->criteria_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prosen')); ?>:</b>
	<?php echo CHtml::encode($data->prosen); ?>
	<br />


</div>