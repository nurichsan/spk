<?php
/* @var $this CriteriaController */
/* @var $model SawCriteria */

$this->breadcrumbs=array(
	'Saw Criterias'=>array('index'),
	$model->criteria_id=>array('view','id'=>$model->criteria_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SawCriteria', 'url'=>array('index')),
	array('label'=>'Create SawCriteria', 'url'=>array('create')),
	array('label'=>'View SawCriteria', 'url'=>array('view', 'id'=>$model->criteria_id)),
	array('label'=>'Manage SawCriteria', 'url'=>array('admin')),
);
?>

<h1>Update SawCriteria <?php echo $model->criteria_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>