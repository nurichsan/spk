<?php

/**
 * This is the model class for table "saw_value".
 *
 * The followings are the available columns in table 'saw_value':
 * @property string $person_id
 * @property string $criteria_id
 * @property string $value
 */
class SawValue extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'saw_value';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('person_id, criteria_id, value', 'required'),
            array('person_id, criteria_id', 'length', 'max' => 5),
            array('value', 'length', 'max' => 14),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('person_id, criteria_id, value', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rel_criteria' => array(self::BELONGS_TO, 'SawCriteria', 'criteria_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'person_id' => 'Person',
            'criteria_id' => 'Criteria',
            'value' => 'Value',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('person_id', $this->person_id, true);
        $criteria->compare('criteria_id', $this->criteria_id, true);
        $criteria->compare('value', $this->value, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SawValue the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
