# Sistem Pendukung Keputusan #
# Metode SAW, AHP & Electre #
* Dikembangkan menggunakan Apache, PHP, MySQL & Bootstrap Template.
* Sesuaikan konfigurasi database pada protected/config/main.php.
* Jika terjadi error seperti berikut : CAssetManager.basePath "D:\xampp2\htdocs\spk\assets" is invalid. Please make sure the directory exists and is writable by the Web server process. Solusi : buat folder dengan nama assets dibawah root direktori aplikasi (setara dengan folder protected).
* Ubah permission pada direktori protected/runtime dengan mode 0777 (khusus pengguna Unix).

#System Requirement
* PHP 5.5
* Apache 2.2
* MySQL

