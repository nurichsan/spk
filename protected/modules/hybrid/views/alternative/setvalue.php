<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-user"></i> <span style="color:blue;font-weight: bold;">Alternative : <?php echo $alt->alternative_id . ' / ' . $alt->alternative_name; ?></span>
        </div>							
    </div>
    <div class="block-content collapse in">
        <?php echo CHtml::form('', 'POST', array('class' => 'form-horizontal')); ?>
        <div class="form-body">
            <?php foreach ($criteria as $row) : ?>                
                <div class="control-group">
                    <label class="control-label"><?php echo $row->label.'/'. $row->criteria_name; ?></label>                
                    <div class="controls">                        
                        <?php
                        if ($row->type_field == 'dropdown') {
                            if ($row->criteria_id == 4) {
                                echo CHtml::dropDownList('Value[' . $row->criteria_id . ']', (isset($data[$row->criteria_id]) ? $data[$row->criteria_id] : ''), AhpLang::type2(), array('class' => 'span6 m-wrap', 'empty' => 'Pilih'));
                            } else {
                                echo CHtml::dropDownList('Value[' . $row->criteria_id . ']', (isset($data[$row->criteria_id]) ? $data[$row->criteria_id] : ''), AhpLang::type1(), array('class' => 'span6 m-wrap', 'empty' => 'Pilih'));
                            }
                        } else {
                            echo CHtml::textField('Value[' . $row->criteria_id . ']', (isset($data[$row->criteria_id]) ? $data[$row->criteria_id] : ''), array('size' => 5, 'data-required' => '1', 'class' => 'span6 m-wrap', 'autocomplete'=>'off'));
                        }
                        ?>
                        <span style = "color: red;"></span>
                    </div>
                </div>
            <?php endforeach;
            ?>
            <div class="form-actions">
                <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div><!-- form -->