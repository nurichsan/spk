<?php
/* @var $this PersonController */
/* @var $model Person */
/* @var $form CActiveForm */
?>

<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-user"></i> Person        </div>							
    </div>
    <div class="block-content collapse in">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'person-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'htmlOptions' => array('class' => 'form-horizontal')
                ));
        ?>
        <?php echo $form->errorSummary($model, '', '', array('class' => 'alert alert-danger')); ?>
        <div class="form-body">
            <div class="control-group">
                <label class="control-label">ID</label>                
                <div class="controls">
                    <?php echo $form->textField($model, 'person_id', array('size' => 5, 'maxlength' => 5)); ?>
                    <?php echo $form->error($model, 'person_id', array('class' => 'alert alert-danger')); ?>
                </div>                    
            </div>
            <div class="control-group">
                <label class="control-label">Name</label>                
                <div class="controls">
                    <?php echo $form->textField($model, 'person_name', array('size' => 60, 'maxlength' => 100)); ?>
                    <?php echo $form->error($model, 'person_name', array('class' => 'alert alert-danger')); ?>
                </div>                    
            </div>            
            <div class="form-actions">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Simpan', array('class' => 'btn green')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->