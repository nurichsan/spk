<?php
$cat = AhpCategory::model()->findAll();
$jml_cri = array();
foreach ($cat as $xc) :
    ?>
    <table class="table table-bordered">
        <tr>
            <?php
            $cri = AhpCriteria::model()->findAllByAttributes(array('category_id' => $xc->category_id));
            echo "<th>#</th>";
            foreach ($cri as $xri) {
                echo "<th>" . $xri->label . "</th>";
                $jml_cri[$xri->criteria_id] = 0;
            }
            ?>
        </tr>
        <?php
        $data = TopsisCalculation::getTabel($xc->category_id);
        $crt_alt = new CDbCriteria();
        $crt_alt->join = "INNER JOIN ahp_mapping a ON a.alternative_id=t.alternative_id";
        $crt_alt->addColumnCondition(array('a.category_id'=>$xc->category_id));
        $altv = AhpAlternative::model()->findAll($crt_alt);
        foreach ($altv as $xal) {
            echo '<tr>';
            echo '<td>' . $xal->alternative_name . '</td>';
            foreach ($cri as $zri) {
                echo '<td align=right>' . $data[$xal->alternative_id][$zri->criteria_id] . '</td>';
                $jml_cri[$zri->criteria_id] += pow($data[$xal->alternative_id][$zri->criteria_id],2);
            }
            echo '</tr>';
        }
        echo '<tr>';
        echo '<td>Jumlah</td>';
        foreach ($cri as $xri) {
            $nilai = sqrt($jml_cri[$xri->criteria_id]);
            echo '<td align=right>' . (number_format($nilai,3,',','.')) . '</td>';
            AhpCriteria::model()->updateByPk($xri->criteria_id, array('jml_topsis'=>$nilai));
        }
        echo '</tr>';
        ?>
    </table>
<?php endforeach; ?>

