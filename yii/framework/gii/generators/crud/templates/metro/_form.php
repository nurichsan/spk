<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="portlet box green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-reorder"></i> <?php echo $this->modelClass; ?>
        </div>							
    </div>
    <div class="portlet-body form">

        <?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'" . $this->class2id($this->modelClass) . "-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>\n"; ?>
        <?php echo "<?php echo \$form->errorSummary(\$model, '', '', array('class' => 'alert alert-danger')); ?>\n"; ?>
        <div class="form-body">
            <?php
            foreach ($this->tableSchema->columns as $column) {
                if ($column->autoIncrement)
                    continue;
                ?>
                <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo "<?php echo " . $this->generateActiveLabel($this->modelClass, $column) . "; ?>\n"; ?></label>                
                    <div class="col-md-9">
                        <?php echo "<?php echo " . $this->generateActiveField($this->modelClass, $column) . "; ?>\n"; ?>
                        <?php echo "<?php echo \$form->error(\$model,'{$column->name}', array('class'=>'alert alert-danger')); ?>\n"; ?>
                    </div>                    
                </div>
                <?php
            }
            ?>
            <div class="form-actions fluid">
                <div class="col-md-offset-3 col-md-9">
                    <?php echo "<?php echo CHtml::submitButton(\$model->isNewRecord ? 'Simpan' : 'Simpan', array('class'=>'btn green')); ?>\n"; ?>
                </div>
            </div>
        </div>
        <?php echo "<?php \$this->endWidget(); ?>\n"; ?>
    </div>
</div><!-- form -->