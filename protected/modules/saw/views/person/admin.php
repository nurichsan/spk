<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs = array(
    'People' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#person-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-user"></i>
            People        
        </div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <?php
            echo CHtml::link('Create', Yii::app()->controller->createUrl("create"), array('class' => 'btn btn-primary'));
            ?>            
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'person-grid',
                'dataProvider' => $model->search(),
                //'filter' => $model,
                'itemsCssClass' => 'table table-striped table-bordered',
                'columns' => array(
                    'person_id',
                    'person_name',
                    array(
                        'class' => 'MyButtonColumn',
                        'template' => '{view} {delete}',
                        'buttons' => array(
                            'view' => array(
                                'url' => 'Yii::app()->controller->createUrl("value", array("id"=>$data->person_id))',
                            )
                        )
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</div>