-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 19, 2017 at 05:17 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk0518`
--

-- --------------------------------------------------------

--
-- Table structure for table `ahp_alternative`
--

CREATE TABLE `ahp_alternative` (
  `alternative_id` int(11) NOT NULL,
  `alternative_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ahp_alternative`
--

INSERT INTO `ahp_alternative` (`alternative_id`, `alternative_name`) VALUES
(1, 'Petugas 1'),
(2, 'Petugas 2'),
(3, 'Petugas 3'),
(4, 'Petugas 4'),
(5, 'Petugas 5'),
(6, 'Petugas 6'),
(7, 'Petugas 7'),
(8, 'Petugas 8'),
(9, 'Petugas 9'),
(10, 'Petugas 10'),
(11, 'Petugas 11');

-- --------------------------------------------------------

--
-- Table structure for table `ahp_category`
--

CREATE TABLE `ahp_category` (
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ahp_category`
--

INSERT INTO `ahp_category` (`category_id`, `category_name`) VALUES
(1, 'Umum'),
(2, 'Ruangan'),
(3, 'Halaman');

-- --------------------------------------------------------

--
-- Table structure for table `ahp_consistency_index`
--

CREATE TABLE `ahp_consistency_index` (
  `item` int(11) NOT NULL DEFAULT '0',
  `value` decimal(14,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ahp_consistency_index`
--

INSERT INTO `ahp_consistency_index` (`item`, `value`) VALUES
(1, '0.00'),
(2, '0.00'),
(3, '0.58'),
(4, '0.90'),
(5, '1.12'),
(6, '1.24'),
(7, '1.32'),
(8, '1.41'),
(9, '1.45'),
(10, '1.49'),
(11, '1.51'),
(12, '1.48'),
(13, '1.56'),
(14, '1.57'),
(15, '1.59');

-- --------------------------------------------------------

--
-- Table structure for table `ahp_criteria`
--

CREATE TABLE `ahp_criteria` (
  `criteria_id` int(11) NOT NULL,
  `criteria_name` varchar(50) DEFAULT NULL,
  `label` varchar(3) DEFAULT NULL,
  `type_field` enum('dropdown','textfield') DEFAULT 'textfield',
  `type` enum('benefit','cost') DEFAULT 'benefit',
  `category_id` bigint(20) DEFAULT NULL,
  `bobot` decimal(14,3) DEFAULT NULL,
  `jml_topsis` decimal(14,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ahp_criteria`
--

INSERT INTO `ahp_criteria` (`criteria_id`, `criteria_name`, `label`, `type_field`, `type`, `category_id`, `bobot`, `jml_topsis`) VALUES
(1, 'Kesetiaan', 'U1', 'textfield', 'benefit', 1, '0.163', '171.00'),
(2, 'Prestasi Kerja', 'U2', 'textfield', 'benefit', 1, '0.134', '143.00'),
(3, 'Tanggung Jawab', 'U3', 'textfield', 'benefit', 1, '0.125', '177.00'),
(4, 'Ketaatan', 'U4', 'textfield', 'benefit', 1, '0.124', '184.00'),
(5, 'Kejujuran', 'U5', 'textfield', 'benefit', 1, '0.087', '164.00'),
(6, 'Kerjasama', 'U6', 'textfield', 'benefit', 1, '0.083', '183.00'),
(7, 'Loyalitas', 'U7', 'textfield', 'benefit', 1, '0.076', '179.00'),
(8, 'Kedisplinan', 'U8', 'textfield', 'benefit', 1, '0.066', '179.00'),
(9, 'Hubungan Antar Manusia', 'U9', 'textfield', 'benefit', 1, '0.056', '174.00'),
(10, 'Kreatifitas', 'U10', 'textfield', 'benefit', 1, '0.048', '168.00'),
(11, 'Prestasi', 'U11', 'textfield', 'benefit', 1, '0.038', '180.00'),
(12, 'Mengeluarkan Sampah', 'R1', 'textfield', 'benefit', 2, '0.202', '163.00'),
(13, 'Membersihkan Tempat Sampah', 'R2', 'textfield', 'benefit', 2, '0.143', '154.00'),
(14, 'Membersihkan Furniture', 'R3', 'textfield', 'benefit', 2, '0.150', '174.00'),
(15, 'Menyapu dan Mengepel Lantai', 'R4', 'textfield', 'benefit', 2, '0.105', '166.00'),
(16, 'Membersihkan KM, WC dan Watafel', 'R5', 'textfield', 'benefit', 2, '0.126', '145.00'),
(17, 'Membersihkan Pintu, Kaca dan Jendela', 'R6', 'textfield', 'benefit', 2, '0.096', '175.00'),
(18, 'Membersihkan Dindin, Ventilasi dan Langit-Langit', 'R7', 'textfield', 'benefit', 2, '0.097', '179.00'),
(19, 'Pengelolaan Peralatan Kerja', 'R8', 'textfield', 'benefit', 2, '0.081', '168.00'),
(20, 'Menyapu', 'H1', 'textfield', 'benefit', 3, '0.288', '173.00'),
(21, 'Membersihkan Saluran Hujan', 'H2', 'textfield', 'benefit', 3, '0.205', '152.00'),
(22, 'Membuang Sampah ke TPS', 'H3', 'textfield', 'benefit', 3, '0.147', '174.00'),
(23, 'Mencuci Tampat Sampah', 'H4', 'textfield', 'benefit', 3, '0.116', '179.00'),
(24, 'Menguras Kolam Ikan', 'H5', 'textfield', 'benefit', 3, '0.099', '145.00'),
(25, 'Merapihkan Taman', 'H6', 'textfield', 'benefit', 3, '0.053', '174.00'),
(26, 'Kerja Bakti', 'H7', 'textfield', 'benefit', 3, '0.048', '179.00'),
(27, 'Pengelolaan Peralatan Kerja', 'H8', 'textfield', 'benefit', 3, '0.043', '161.00');

-- --------------------------------------------------------

--
-- Table structure for table `ahp_pair_wise`
--

CREATE TABLE `ahp_pair_wise` (
  `category_id` int(11) NOT NULL,
  `criteria_1` int(11) NOT NULL DEFAULT '0',
  `criteria_2` int(11) NOT NULL DEFAULT '0',
  `value` decimal(14,3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ahp_pair_wise`
--

INSERT INTO `ahp_pair_wise` (`category_id`, `criteria_1`, `criteria_2`, `value`) VALUES
(1, 1, 1, '1.000'),
(1, 1, 2, '3.003'),
(1, 1, 3, '2.000'),
(1, 1, 4, '2.000'),
(1, 1, 5, '3.003'),
(1, 1, 6, '2.000'),
(1, 1, 7, '2.000'),
(1, 1, 8, '1.000'),
(1, 1, 9, '2.000'),
(1, 1, 10, '2.000'),
(1, 1, 11, '3.000'),
(1, 2, 1, '0.333'),
(1, 2, 2, '1.000'),
(1, 2, 3, '3.003'),
(1, 2, 4, '2.000'),
(1, 2, 5, '2.000'),
(1, 2, 6, '1.000'),
(1, 2, 7, '2.000'),
(1, 2, 8, '2.000'),
(1, 2, 9, '3.003'),
(1, 2, 10, '2.000'),
(1, 2, 11, '2.000'),
(1, 3, 1, '0.500'),
(1, 3, 2, '0.333'),
(1, 3, 3, '1.000'),
(1, 3, 4, '2.000'),
(1, 3, 5, '2.000'),
(1, 3, 6, '3.003'),
(1, 3, 7, '2.000'),
(1, 3, 8, '2.000'),
(1, 3, 9, '2.000'),
(1, 3, 10, '3.003'),
(1, 3, 11, '2.000'),
(1, 4, 1, '0.500'),
(1, 4, 2, '0.500'),
(1, 4, 3, '0.500'),
(1, 4, 4, '1.000'),
(1, 4, 5, '3.003'),
(1, 4, 6, '2.000'),
(1, 4, 7, '3.003'),
(1, 4, 8, '3.003'),
(1, 4, 9, '2.000'),
(1, 4, 10, '2.000'),
(1, 4, 11, '3.000'),
(1, 5, 1, '0.333'),
(1, 5, 2, '0.500'),
(1, 5, 3, '0.500'),
(1, 5, 4, '0.333'),
(1, 5, 5, '1.000'),
(1, 5, 6, '2.000'),
(1, 5, 7, '2.000'),
(1, 5, 8, '2.000'),
(1, 5, 9, '2.000'),
(1, 5, 10, '2.000'),
(1, 5, 11, '2.000'),
(1, 6, 1, '0.500'),
(1, 6, 2, '1.000'),
(1, 6, 3, '0.333'),
(1, 6, 4, '0.500'),
(1, 6, 5, '0.500'),
(1, 6, 6, '1.000'),
(1, 6, 7, '2.000'),
(1, 6, 8, '3.003'),
(1, 6, 9, '1.000'),
(1, 6, 10, '2.000'),
(1, 6, 11, '2.000'),
(1, 7, 1, '0.500'),
(1, 7, 2, '0.500'),
(1, 7, 3, '0.500'),
(1, 7, 4, '0.333'),
(1, 7, 5, '0.500'),
(1, 7, 6, '0.500'),
(1, 7, 7, '1.000'),
(1, 7, 8, '2.000'),
(1, 7, 9, '2.000'),
(1, 7, 10, '3.003'),
(1, 7, 11, '3.000'),
(1, 8, 1, '1.000'),
(1, 8, 2, '0.500'),
(1, 8, 3, '0.500'),
(1, 8, 4, '0.333'),
(1, 8, 5, '0.500'),
(1, 8, 6, '0.333'),
(1, 8, 7, '0.500'),
(1, 8, 8, '1.000'),
(1, 8, 9, '3.003'),
(1, 8, 10, '2.000'),
(1, 8, 11, '2.000'),
(1, 9, 1, '0.500'),
(1, 9, 2, '0.333'),
(1, 9, 3, '0.500'),
(1, 9, 4, '0.500'),
(1, 9, 5, '0.500'),
(1, 9, 6, '1.000'),
(1, 9, 7, '0.500'),
(1, 9, 8, '0.333'),
(1, 9, 9, '1.000'),
(1, 9, 10, '2.000'),
(1, 9, 11, '2.000'),
(1, 10, 1, '0.500'),
(1, 10, 2, '0.500'),
(1, 10, 3, '0.333'),
(1, 10, 4, '0.500'),
(1, 10, 5, '0.500'),
(1, 10, 6, '0.500'),
(1, 10, 7, '0.333'),
(1, 10, 8, '0.500'),
(1, 10, 9, '0.500'),
(1, 10, 10, '1.000'),
(1, 10, 11, '3.000'),
(1, 11, 1, '0.333'),
(1, 11, 2, '0.500'),
(1, 11, 3, '0.500'),
(1, 11, 4, '0.333'),
(1, 11, 5, '0.500'),
(1, 11, 6, '0.500'),
(1, 11, 7, '0.333'),
(1, 11, 8, '0.500'),
(1, 11, 9, '0.500'),
(1, 11, 10, '0.333'),
(1, 11, 11, '1.000'),
(2, 12, 12, '1.000'),
(2, 12, 13, '2.000'),
(2, 12, 14, '2.000'),
(2, 12, 15, '2.000'),
(2, 12, 16, '1.000'),
(2, 12, 17, '2.000'),
(2, 12, 18, '2.000'),
(2, 12, 19, '2.000'),
(2, 13, 12, '0.500'),
(2, 13, 13, '1.000'),
(2, 13, 14, '2.000'),
(2, 13, 15, '1.000'),
(2, 13, 16, '2.000'),
(2, 13, 17, '2.000'),
(2, 13, 18, '1.000'),
(2, 13, 19, '1.000'),
(2, 14, 12, '0.500'),
(2, 14, 13, '0.500'),
(2, 14, 14, '1.000'),
(2, 14, 15, '3.003'),
(2, 14, 16, '1.000'),
(2, 14, 17, '2.000'),
(2, 14, 18, '2.000'),
(2, 14, 19, '2.000'),
(2, 15, 12, '0.500'),
(2, 15, 13, '1.000'),
(2, 15, 14, '0.333'),
(2, 15, 15, '1.000'),
(2, 15, 16, '2.000'),
(2, 15, 17, '1.000'),
(2, 15, 18, '1.000'),
(2, 15, 19, '1.000'),
(2, 16, 12, '1.000'),
(2, 16, 13, '0.500'),
(2, 16, 14, '1.000'),
(2, 16, 15, '0.500'),
(2, 16, 16, '1.000'),
(2, 16, 17, '3.003'),
(2, 16, 18, '2.000'),
(2, 16, 19, '1.000'),
(2, 17, 12, '0.500'),
(2, 17, 13, '0.500'),
(2, 17, 14, '0.500'),
(2, 17, 15, '1.000'),
(2, 17, 16, '0.333'),
(2, 17, 17, '1.000'),
(2, 17, 18, '2.000'),
(2, 17, 19, '2.000'),
(2, 18, 12, '0.500'),
(2, 18, 13, '1.000'),
(2, 18, 14, '0.500'),
(2, 18, 15, '1.000'),
(2, 18, 16, '0.500'),
(2, 18, 17, '0.500'),
(2, 18, 18, '1.000'),
(2, 18, 19, '3.000'),
(2, 19, 12, '0.500'),
(2, 19, 13, '1.000'),
(2, 19, 14, '0.500'),
(2, 19, 15, '1.000'),
(2, 19, 16, '1.000'),
(2, 19, 17, '0.500'),
(2, 19, 18, '0.333'),
(2, 19, 19, '1.000'),
(3, 20, 20, '1.000'),
(3, 20, 21, '2.000'),
(3, 20, 22, '3.003'),
(3, 20, 23, '4.000'),
(3, 20, 24, '3.003'),
(3, 20, 25, '2.000'),
(3, 20, 26, '5.000'),
(3, 20, 27, '5.988'),
(3, 21, 20, '0.500'),
(3, 21, 21, '1.000'),
(3, 21, 22, '2.000'),
(3, 21, 23, '3.003'),
(3, 21, 24, '3.003'),
(3, 21, 25, '4.000'),
(3, 21, 26, '2.000'),
(3, 21, 27, '4.000'),
(3, 22, 20, '0.333'),
(3, 22, 21, '0.500'),
(3, 22, 22, '1.000'),
(3, 22, 23, '2.000'),
(3, 22, 24, '2.000'),
(3, 22, 25, '3.003'),
(3, 22, 26, '5.000'),
(3, 22, 27, '2.000'),
(3, 23, 20, '0.250'),
(3, 23, 21, '0.333'),
(3, 23, 22, '0.500'),
(3, 23, 23, '1.000'),
(3, 23, 24, '4.000'),
(3, 23, 25, '3.003'),
(3, 23, 26, '3.003'),
(3, 23, 27, '2.000'),
(3, 24, 20, '0.333'),
(3, 24, 21, '0.333'),
(3, 24, 22, '0.500'),
(3, 24, 23, '0.250'),
(3, 24, 24, '1.000'),
(3, 24, 25, '5.000'),
(3, 24, 26, '4.000'),
(3, 24, 27, '3.003'),
(3, 25, 20, '0.500'),
(3, 25, 21, '0.250'),
(3, 25, 22, '0.333'),
(3, 25, 23, '0.333'),
(3, 25, 24, '0.200'),
(3, 25, 25, '1.000'),
(3, 25, 26, '2.000'),
(3, 25, 27, '1.000'),
(3, 26, 20, '0.200'),
(3, 26, 21, '0.500'),
(3, 26, 22, '0.200'),
(3, 26, 23, '0.333'),
(3, 26, 24, '0.250'),
(3, 26, 25, '0.500'),
(3, 26, 26, '1.000'),
(3, 26, 27, '3.003'),
(3, 27, 20, '0.167'),
(3, 27, 21, '0.250'),
(3, 27, 22, '0.500'),
(3, 27, 23, '0.500'),
(3, 27, 24, '0.333'),
(3, 27, 25, '1.000'),
(3, 27, 26, '0.333'),
(3, 27, 27, '1.000');

-- --------------------------------------------------------

--
-- Table structure for table `ahp_value`
--

CREATE TABLE `ahp_value` (
  `alternative_id` int(11) NOT NULL DEFAULT '0',
  `criteria_id` int(11) NOT NULL DEFAULT '0',
  `value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ahp_value`
--

INSERT INTO `ahp_value` (`alternative_id`, `criteria_id`, `value`) VALUES
(1, 1, 87),
(1, 2, 71),
(1, 3, 87),
(1, 4, 91),
(1, 5, 88),
(1, 6, 90),
(1, 7, 90),
(1, 8, 90),
(1, 9, 81),
(1, 10, 89),
(1, 11, 91),
(1, 12, 80),
(1, 13, 87),
(1, 14, 87),
(1, 15, 87),
(1, 16, 71),
(1, 17, 88),
(1, 18, 90),
(1, 19, 81),
(1, 20, 89),
(1, 21, 80),
(1, 22, 87),
(1, 23, 86),
(1, 24, 71),
(1, 25, 81),
(1, 26, 90),
(1, 27, 80),
(2, 1, 84),
(2, 2, 72),
(2, 3, 90),
(2, 4, 93),
(2, 5, 76),
(2, 6, 93),
(2, 7, 89),
(2, 8, 89),
(2, 9, 93),
(2, 10, 79),
(2, 11, 89),
(2, 12, 83),
(2, 13, 67),
(2, 14, 87),
(2, 15, 79),
(2, 16, 74),
(2, 17, 87),
(2, 18, 89),
(2, 19, 87),
(2, 20, 84),
(2, 21, 72),
(2, 22, 87),
(2, 23, 93),
(2, 24, 74),
(2, 25, 93),
(2, 26, 89),
(2, 27, 81);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `person_id` varchar(5) NOT NULL,
  `person_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `person_name`) VALUES
('PS001', 'RONALDO'),
('PS002', 'RAMOS'),
('PS003', 'JAMES'),
('PS004', 'BENZEMA'),
('PS005', 'BALE'),
('PS006', 'VARANE'),
('PS007', 'MARCELLO'),
('PS008', 'CARVAJAL'),
('PS009', 'KROOS'),
('PS010', 'MODRIC'),
('PS011', 'NAVAS'),
('PS012', 'CASEMIRO'),
('PS013', 'DANILO'),
('PS014', 'ARBELOA'),
('PS015', 'NACHO'),
('PS016', 'KOVACIC');

-- --------------------------------------------------------

--
-- Table structure for table `saw_criteria`
--

CREATE TABLE `saw_criteria` (
  `criteria_id` varchar(5) NOT NULL DEFAULT '',
  `criteria_name` varchar(100) DEFAULT NULL,
  `prosen` decimal(14,2) DEFAULT NULL,
  `type` enum('benefit','cost','','') NOT NULL DEFAULT 'benefit',
  `no_urut` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saw_criteria`
--

INSERT INTO `saw_criteria` (`criteria_id`, `criteria_name`, `prosen`, `type`, `no_urut`) VALUES
('C1', 'KELINCAHAN', '9.00', 'benefit', 1),
('C10', 'KOGNITIF', '9.00', 'benefit', 10),
('C11', 'EMOSIONAL', '5.00', 'cost', 11),
('C2', 'KESEIMBANGAN', '10.00', 'benefit', 2),
('C3', 'KECEPATAN REAKSI', '11.00', 'benefit', 3),
('C4', 'PASSING BAWAH', '12.00', 'benefit', 4),
('C5', 'PASSING ATAS', '11.00', 'benefit', 5),
('C6', 'DRIBBLING', '8.00', 'benefit', 6),
('C7', 'SHOOTING', '10.00', 'benefit', 7),
('C8', 'HEADING', '8.00', 'benefit', 8),
('C9', 'TACKLING', '7.00', 'cost', 9);

-- --------------------------------------------------------

--
-- Table structure for table `saw_value`
--

CREATE TABLE `saw_value` (
  `person_id` varchar(5) NOT NULL,
  `criteria_id` varchar(5) NOT NULL,
  `value` decimal(14,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saw_value`
--

INSERT INTO `saw_value` (`person_id`, `criteria_id`, `value`) VALUES
('PS001', 'C1', '90.00'),
('PS001', 'C10', '95.00'),
('PS001', 'C11', '10.00'),
('PS001', 'C2', '85.00'),
('PS001', 'C3', '90.00'),
('PS001', 'C4', '70.00'),
('PS001', 'C5', '80.00'),
('PS001', 'C6', '90.00'),
('PS001', 'C7', '96.00'),
('PS001', 'C8', '90.00'),
('PS001', 'C9', '60.00'),
('PS002', 'C1', '75.00'),
('PS002', 'C10', '80.00'),
('PS002', 'C11', '70.00'),
('PS002', 'C2', '89.00'),
('PS002', 'C3', '88.00'),
('PS002', 'C4', '80.00'),
('PS002', 'C5', '90.00'),
('PS002', 'C6', '70.00'),
('PS002', 'C7', '75.00'),
('PS002', 'C8', '80.00'),
('PS002', 'C9', '90.00'),
('PS003', 'C1', '80.00'),
('PS003', 'C10', '87.00'),
('PS003', 'C11', '40.00'),
('PS003', 'C2', '87.00'),
('PS003', 'C3', '86.00'),
('PS003', 'C4', '90.00'),
('PS003', 'C5', '95.00'),
('PS003', 'C6', '85.00'),
('PS003', 'C7', '89.00'),
('PS003', 'C8', '85.00'),
('PS003', 'C9', '60.00'),
('PS004', 'C1', '87.00'),
('PS004', 'C10', '75.00'),
('PS004', 'C11', '60.00'),
('PS004', 'C2', '80.00'),
('PS004', 'C3', '98.00'),
('PS004', 'C4', '89.00'),
('PS004', 'C5', '89.00'),
('PS004', 'C6', '83.00'),
('PS004', 'C7', '85.00'),
('PS004', 'C8', '91.00'),
('PS004', 'C9', '30.00'),
('PS005', 'C1', '90.00'),
('PS005', 'C10', '70.00'),
('PS005', 'C11', '50.00'),
('PS005', 'C2', '85.00'),
('PS005', 'C3', '88.00'),
('PS005', 'C4', '70.00'),
('PS005', 'C5', '85.00'),
('PS005', 'C6', '85.00'),
('PS005', 'C7', '90.00'),
('PS005', 'C8', '86.00'),
('PS005', 'C9', '50.00'),
('PS006', 'C1', '70.00'),
('PS006', 'C10', '82.00'),
('PS006', 'C11', '30.00'),
('PS006', 'C2', '90.00'),
('PS006', 'C3', '87.00'),
('PS006', 'C4', '75.00'),
('PS006', 'C5', '70.00'),
('PS006', 'C6', '70.00'),
('PS006', 'C7', '60.00'),
('PS006', 'C8', '95.00'),
('PS006', 'C9', '55.00'),
('PS007', 'C1', '89.00'),
('PS007', 'C10', '76.00'),
('PS007', 'C11', '80.00'),
('PS007', 'C2', '83.00'),
('PS007', 'C3', '89.00'),
('PS007', 'C4', '91.00'),
('PS007', 'C5', '90.00'),
('PS007', 'C6', '88.00'),
('PS007', 'C7', '75.00'),
('PS007', 'C8', '50.00'),
('PS007', 'C9', '88.00'),
('PS008', 'C1', '86.00'),
('PS008', 'C10', '65.00'),
('PS008', 'C11', '40.00'),
('PS008', 'C2', '75.00'),
('PS008', 'C3', '88.00'),
('PS008', 'C4', '87.00'),
('PS008', 'C5', '89.00'),
('PS008', 'C6', '80.00'),
('PS008', 'C7', '80.00'),
('PS008', 'C8', '60.00'),
('PS008', 'C9', '87.00'),
('PS009', 'C1', '70.00'),
('PS009', 'C10', '86.00'),
('PS009', 'C11', '40.00'),
('PS009', 'C2', '90.00'),
('PS009', 'C3', '80.00'),
('PS009', 'C4', '95.00'),
('PS009', 'C5', '95.00'),
('PS009', 'C6', '70.00'),
('PS009', 'C7', '75.00'),
('PS009', 'C8', '86.00'),
('PS009', 'C9', '65.00'),
('PS010', 'C1', '89.00'),
('PS010', 'C10', '65.00'),
('PS010', 'C11', '56.00'),
('PS010', 'C2', '75.00'),
('PS010', 'C3', '92.00'),
('PS010', 'C4', '94.00'),
('PS010', 'C5', '95.00'),
('PS010', 'C6', '80.00'),
('PS010', 'C7', '87.00'),
('PS010', 'C8', '40.00'),
('PS010', 'C9', '70.00'),
('PS011', 'C1', '75.00'),
('PS011', 'C10', '100.00'),
('PS011', 'C11', '20.00'),
('PS011', 'C2', '87.00'),
('PS011', 'C3', '88.00'),
('PS011', 'C4', '70.00'),
('PS011', 'C5', '65.00'),
('PS011', 'C6', '50.00'),
('PS011', 'C7', '60.00'),
('PS011', 'C8', '89.00'),
('PS011', 'C9', '10.00'),
('PS012', 'C1', '70.00'),
('PS012', 'C10', '88.00'),
('PS012', 'C11', '60.00'),
('PS012', 'C2', '90.00'),
('PS012', 'C3', '75.00'),
('PS012', 'C4', '86.00'),
('PS012', 'C5', '87.00'),
('PS012', 'C6', '60.00'),
('PS012', 'C7', '70.00'),
('PS012', 'C8', '90.00'),
('PS012', 'C9', '40.00'),
('PS013', 'C1', '60.00'),
('PS013', 'C10', '78.00'),
('PS013', 'C11', '50.00'),
('PS013', 'C2', '50.00'),
('PS013', 'C3', '80.00'),
('PS013', 'C4', '77.00'),
('PS013', 'C5', '86.00'),
('PS013', 'C6', '88.00'),
('PS013', 'C7', '67.00'),
('PS013', 'C8', '70.00'),
('PS013', 'C9', '77.00'),
('PS014', 'C1', '50.00'),
('PS014', 'C10', '86.00'),
('PS014', 'C11', '30.00'),
('PS014', 'C2', '60.00'),
('PS014', 'C3', '65.00'),
('PS014', 'C4', '70.00'),
('PS014', 'C5', '75.00'),
('PS014', 'C6', '65.00'),
('PS014', 'C7', '70.00'),
('PS014', 'C8', '75.00'),
('PS014', 'C9', '80.00'),
('PS015', 'C1', '70.00'),
('PS015', 'C10', '75.00'),
('PS015', 'C11', '30.00'),
('PS015', 'C2', '75.00'),
('PS015', 'C3', '75.00'),
('PS015', 'C4', '70.00'),
('PS015', 'C5', '80.00'),
('PS015', 'C6', '60.00'),
('PS015', 'C7', '75.00'),
('PS015', 'C8', '70.00'),
('PS015', 'C9', '65.00'),
('PS016', 'C1', '75.00'),
('PS016', 'C10', '75.00'),
('PS016', 'C11', '80.00'),
('PS016', 'C2', '85.00'),
('PS016', 'C3', '77.00'),
('PS016', 'C4', '86.00'),
('PS016', 'C5', '80.00'),
('PS016', 'C6', '76.00'),
('PS016', 'C7', '77.00'),
('PS016', 'C8', '76.00'),
('PS016', 'C9', '60.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ahp_alternative`
--
ALTER TABLE `ahp_alternative`
  ADD PRIMARY KEY (`alternative_id`);

--
-- Indexes for table `ahp_category`
--
ALTER TABLE `ahp_category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `category_id` (`category_id`);

--
-- Indexes for table `ahp_consistency_index`
--
ALTER TABLE `ahp_consistency_index`
  ADD PRIMARY KEY (`item`);

--
-- Indexes for table `ahp_criteria`
--
ALTER TABLE `ahp_criteria`
  ADD PRIMARY KEY (`criteria_id`),
  ADD KEY `PCategory` (`category_id`);

--
-- Indexes for table `ahp_pair_wise`
--
ALTER TABLE `ahp_pair_wise`
  ADD PRIMARY KEY (`criteria_1`,`criteria_2`);

--
-- Indexes for table `ahp_value`
--
ALTER TABLE `ahp_value`
  ADD PRIMARY KEY (`alternative_id`,`criteria_id`),
  ADD KEY `criteria_id` (`criteria_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `saw_criteria`
--
ALTER TABLE `saw_criteria`
  ADD PRIMARY KEY (`criteria_id`);

--
-- Indexes for table `saw_value`
--
ALTER TABLE `saw_value`
  ADD PRIMARY KEY (`person_id`,`criteria_id`),
  ADD KEY `criteria_id` (`criteria_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ahp_alternative`
--
ALTER TABLE `ahp_alternative`
  MODIFY `alternative_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ahp_category`
--
ALTER TABLE `ahp_category`
  MODIFY `category_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ahp_criteria`
--
ALTER TABLE `ahp_criteria`
  MODIFY `criteria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ahp_value`
--
ALTER TABLE `ahp_value`
  ADD CONSTRAINT `ahp_value_ibfk_1` FOREIGN KEY (`alternative_id`) REFERENCES `ahp_alternative` (`alternative_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ahp_value_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `ahp_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `saw_value`
--
ALTER TABLE `saw_value`
  ADD CONSTRAINT `saw_value_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `saw_value_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `saw_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
