<?php
/* @var $this AlternativeController */
/* @var $model AhpAlternative */

$this->breadcrumbs = array(
    'Ahp Alternatives' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List AhpAlternative', 'url' => array('index')),
    array('label' => 'Create AhpAlternative', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#ahp-alternative-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>
<div class="alert alert-info">
    <h4>Info!! </h4>               
    Jika menambah kriteria atau <i>alternative</i> baru. Pastikan setiap <i>alternative</i> juga memiliki nilai terhadap kriteria.
</div>
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-list"></i>
            Ahp Alternative
        </div>
    </div>
    <div class="block-content collapse in">        
        <div class="span12">
            <?php echo CHtml::link(CHtml::htmlButton('<i class="icon icon-folder-open"></i> Create', array('class' => 'btn btn-success')), Yii::app()->controller->createUrl("create")); ?>            
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'ahp-alternative-grid',
                'dataProvider' => $model->search(),
                //'filter' => $model,
                'itemsCssClass' => 'table table-striped table-bordered',
                'columns' => array(
                    'alternative_id',
                    'alternative_name',
                    array(
                        'type' => 'raw',
                        'value' => 'CHtml::link("set value", Yii::app()->controller->createUrl("setvalue", array("id"=>$data->alternative_id)))',
                    ),
                    array(
                        'class' => 'CButtonColumn',
                        'template' => '{update} {delete}'
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</div>