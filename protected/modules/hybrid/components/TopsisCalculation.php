<?php

/**
 * Description of TopsisCalculation
 *
 * @author nur
 */
class TopsisCalculation {

    public static function getTabel($cat_id) {
        $cr = new CDbCriteria();
        $cr->join = 'inner join ahp_criteria a ON a.criteria_id=t.criteria_id';
        $cr->condition = 'a.category_id = :param';
        $cr->params = array(':param'=>$cat_id);
        $ahp_val = AhpValue::model()->findAll($cr);
        $m_val = array();
        foreach ($ahp_val as $row){
            $m_val[$row->alternative_id][$row->criteria_id] = $row->value;
        }
        return $m_val;
    }

}
