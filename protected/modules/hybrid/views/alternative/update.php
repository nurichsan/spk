<?php
/* @var $this AlternativeController */
/* @var $model AhpAlternative */

$this->breadcrumbs=array(
	'Ahp Alternatives'=>array('index'),
	$model->alternative_id=>array('view','id'=>$model->alternative_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AhpAlternative', 'url'=>array('index')),
	array('label'=>'Create AhpAlternative', 'url'=>array('create')),
	array('label'=>'View AhpAlternative', 'url'=>array('view', 'id'=>$model->alternative_id)),
	array('label'=>'Manage AhpAlternative', 'url'=>array('admin')),
);
?>

<h1>Update AhpAlternative <?php echo $model->alternative_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>