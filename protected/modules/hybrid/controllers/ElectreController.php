<?php

/**
 * Description of ElectreController
 *
 * @author Nur Ichsan
 */
class ElectreController extends Controller {

    public function actionMatrik() {        
        $all_criteria = AhpCriteria::model()->findAll();
        $alternative = AhpAlternative::model()->findAll();
        $matrik = Electre::getMatrik($all_criteria);
        $this->render('matrik', array(
            'matrik' => $matrik,
            'alternative' => $alternative,
            'all_criteria'=>$all_criteria
        ));
    }

}

?>
