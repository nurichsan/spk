<?php
/* @var $this CriteriaController */
/* @var $model SawCriteria */
/* @var $form CActiveForm */
?>

<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-folder-open"></i> Create Criteria</div>							
    </div>
    <div class="block-content collapse in">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'saw-criteria-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'htmlOptions' => array('class' => 'form-horizontal')
                ));
        ?>
        <?php echo $form->errorSummary($model, '', '', array('class' => 'alert alert-danger')); ?>
        <div class="form-body">
            <div class="control-group">
                <label class="control-label"><?php echo $form->labelEx($model, 'criteria_id'); ?>
                </label>                
                <div class="controls">
                    <?php echo $form->textField($model, 'criteria_id', array('size' => 5, 'maxlength' => 5)); ?>
                    <?php echo $form->error($model, 'criteria_id', array('class' => 'alert alert-danger')); ?>
                </div>                    
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo $form->labelEx($model, 'criteria_name'); ?>
                </label>                
                <div class="controls">
                    <?php echo $form->textField($model, 'criteria_name', array('size' => 60, 'maxlength' => 100)); ?>
                    <?php echo $form->error($model, 'criteria_name', array('class' => 'alert alert-danger')); ?>
                </div>                    
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo $form->labelEx($model, 'prosen'); ?>
                </label>                
                <div class="controls">
                    <?php echo $form->textField($model, 'prosen', array('size' => 14, 'maxlength' => 14)); ?>
                    <?php echo $form->error($model, 'prosen', array('class' => 'alert alert-danger')); ?>
                </div>                    
            </div>

            <div class="control-group">
                <label class="control-label"><?php echo $form->labelEx($model, 'type'); ?>
                </label>                
                <div class="controls">
                    <?php echo $form->checkBoxList($model, 'type', array('benefit' => 'benefit', 'cost' => 'cost')); ?>
                    <?php echo $form->error($model, 'type', array('class' => 'alert alert-danger')); ?>
                </div>                    
            </div>

            <div class="form-actions">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Simpan', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->