<?php
/* @var $this CriteriaController */
/* @var $model AhpCriteria */

$this->breadcrumbs=array(
	'Ahp Criterias'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AhpCriteria', 'url'=>array('index')),
	array('label'=>'Manage AhpCriteria', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>