<?php

/* @var $this AlternativeController */
/* @var $model AhpAlternative */

$this->breadcrumbs = array(
    'Ahp Alternatives' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List AhpAlternative', 'url' => array('index')),
    array('label' => 'Manage AhpAlternative', 'url' => array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model' => $model)); ?>