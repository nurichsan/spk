<?php
/* @var $this CriteriaController */
/* @var $model SawCriteria */

$this->breadcrumbs = array(
    'Saw Criteria' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#saw-criteria-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-list"></i>
            Criteria   
        </div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <?php echo CHtml::link('Create', Yii::app()->controller->createUrl("create"), array('class' => 'btn btn-primary')); ?> 
            <?php
            $total = 0;
            foreach ($model->search()->getData() as $row) {
                $total += $row->prosen;
            }
            ?>
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'saw-criteria-grid',
                'dataProvider' => $model->search(),
                //'filter' => $model,
                'itemsCssClass' => 'table table-striped table-bordered',
                'columns' => array(
                    'criteria_id',
                    'criteria_name',
                    array(
                        'name' => 'prosen',
                        'htmlOptions' => array(
                            'style' => 'text-align:right;'
                        ),
                        'footer' => 'Total : ' . $total,
                        'footerHtmlOptions' => array(
                            'style' => 'text-align:right;'
                        )
                    ),
                    'type',
                    array(
                        'class' => 'CButtonColumn',
                        'htmlOptions' => array(
                            'style' => 'width:75px;text-align:center;'
                        )
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</div>