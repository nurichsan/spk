<?php
/* @var $this PersonController */
/* @var $data Person */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('person_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->person_id), array('view', 'id'=>$data->person_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('person_name')); ?>:</b>
	<?php echo CHtml::encode($data->person_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />


</div>