<?php

class SawModule extends CWebModule {

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        Yii::app()->homeUrl = Yii::app()->createUrl('saw');
        Yii::app()->name = "Metode SAW";
        Yii::app()->id = 'saw';
        $this->setImport(array(
            'saw.models.*',
            'saw.components.*',
        ));
        Yii::app()->setComponents(array(            
            'errorHandler' => array(
                'errorAction' => 'saw/default/error'
            )
        ));
//        $this->layoutPath = Yii::getPathOfAlias('saw.views.layouts');
//        $this->layout = 'main';
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

}
