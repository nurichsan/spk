<!DOCTYPE html>
<html class="no-js">

    <head>
        <title><?php echo Yii::app()->name; ?></title>
        <!-- Bootstrap -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/styles.css" rel="stylesheet" media="screen">       
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/sticky-footer-navbar.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link rel="icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.ico" type="image/x-icon" />
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>        
    </head>

    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->name; ?></a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="">
                                <a href="#"> <i class="icon-user"></i> <span style="color: blue; font-weight: bold;">Nur Ichsan</span> (372281) </a>                                
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="">
                                <a href="<?php echo Yii::app()->homeUrl; ?>"><i class="icon-home"></i> Home</a>
                            </li>
                            <li class="">
                                <a href="<?php echo Yii::app()->createUrl($this->module->id . '/person/admin'); ?>"><i class="icon-user"></i> Person</a>                                
                            </li>
                            <li class="">
                                <a href="<?php echo Yii::app()->createUrl($this->module->id . '/criteria/admin'); ?>"><i class="icon-bookmark"></i> Criteria</a>                                
                            </li>                            
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">                

                <div class="span12" id="content">                                                                                
                    <?php echo $content; ?>             
                </div>
            </div>                        
        </div>
        <footer class="footer">                                        
            <div class="navbar navbar-fixed-bottom">                
                <div class="navbar-inner">
                    <div class="container-fluid">     
                        <div class="nav-collapse collapse">
                            <ul class="nav pull-left">
                                <li class="">
                                    <a href="#"> <i class="icon-bullhorn"></i> Dikembangkan sebagai tugas kuliah Sistem Pendukung Keputusan. </a>                                
                                </li>
                            </ul>
                        </div>                        
                    </div>                
                </div>
            </div>
        </footer>
        <!--/.fluid-container-->
        <?php
        $cs = Yii::app()->clientScript;
        $cs->registerCoreScript('jquery');
        ?>        
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendors/easypiechart/jquery.easy-pie-chart.js"></script>        
        <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/form-validation.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/scripts.js"></script>
        <script>        
            jQuery(document).ready(function() {   
                FormValidation.init();
            });
        </script>
    </script>
</body>

</html>