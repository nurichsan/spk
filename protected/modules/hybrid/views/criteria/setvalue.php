<div class="span4">
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-folder-close"></i> <span style="color:blue;font-weight: bold;">Criteria : <?php echo $model->label . ' / ' . $model->criteria_name; ?></span>
            </div>							
        </div>
        <div class="block-content collapse in">
            <?php echo CHtml::form('', 'POST', array('class' => 'form-horizontal')); ?>
            <div class="form-body">
                <?php foreach ($criteria as $row) : ?>                
                    <div class="control-group">
                        <label class="control-label"><?php echo CHtml::link($row->label . '/' . $row->criteria_name, Yii::app()->controller->createUrl('setvalue', array('id' => $row->criteria_id, 'cat' => $row->category_id))); ?></label>                
                        <div class="controls">                        
                            <?php echo CHtml::textField('Value[' . $row->criteria_id . ']', (isset($data[$row->criteria_id]) ? $data[$row->criteria_id] : ''), array('size' => 5, 'data-required' => '1', 'class' => 'span6 m-wrap', 'autocomplete' => 'off')); ?>
                            <span style = "color: red;"></span>
                        </div>
                    </div>
                <?php endforeach;
                ?>
                <div class="form-actions">
                    <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-success')); ?>
                </div>
            </div>
            <?php echo CHtml::endForm(); ?>
        </div>
    </div><!-- form -->
</div>
<div class="span8">
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">Pairwise Comparison Table</span>
            </div>							
        </div>
        <div class="block-content collapse in">            
            <div class="form-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <?php
                        $sum = array();
                        foreach ($all_criteria as $td) {
                            echo "<th>" . $td->label . "</th>";
                            $sum[$td->criteria_id] = 0;
                        }
                        ?>
                    </tr>
                    <?php
                    foreach ($all_criteria as $val1) {
                        echo "<tr>";
                        echo "<td><span style='font-weight:bold;'>" . $val1->label . "</td>";
                        foreach ($all_criteria as $val2) {
                            if ($val1->criteria_id == $val2->criteria_id) {
                                echo "<td align=right><span style='font-weight:bold;color:red;'>" . (isset($pair_wise[$val1->criteria_id][$val2->criteria_id]) ? $pair_wise[$val1->criteria_id][$val2->criteria_id] : '') . "</span></td>";
                            } else {
                                echo "<td align=right>" . (isset($pair_wise[$val1->criteria_id][$val2->criteria_id]) ? $pair_wise[$val1->criteria_id][$val2->criteria_id] : '') . "</td>";
                            }
                            $sum[$val2->criteria_id] += (isset($pair_wise[$val1->criteria_id][$val2->criteria_id]) ? $pair_wise[$val1->criteria_id][$val2->criteria_id] : 0);
                        }
                        echo "</tr>";
                    }
                    echo "<tr style='color:blue;font-weight:bold;'>";
                    echo "<td align=right>Jumlah</td>";
                    foreach ($all_criteria as $val3) {
                        echo "<td align=right>" . $sum[$val3->criteria_id] . "</td>";
                    }
                    echo "</tr>";
                    ?>
                </table>
            </div>
            <?php echo CHtml::endForm(); ?>
        </div>
    </div><!-- form -->
</div>
