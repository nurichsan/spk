<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'People'=>array('index'),
	$model->person_id,
);

$this->menu=array(
array('label'=>'List Person', 'url'=>array('index')),
array('label'=>'Create Person', 'url'=>array('create')),
array('label'=>'Update Person', 'url'=>array('update', 'id'=>$model->person_id)),
array('label'=>'Delete Person', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->person_id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Person', 'url'=>array('admin')),
);
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-search"></i>
            View Person #<?php echo $model->person_id; ?>        </div>
    </div>
    <div class="portlet-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'person_id',
		'person_name',
		'email',
        ),
        )); ?>
    </div>
</div>