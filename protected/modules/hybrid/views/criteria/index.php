<?php
/* @var $this CriteriaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ahp Criterias',
);

$this->menu=array(
	array('label'=>'Create AhpCriteria', 'url'=>array('create')),
	array('label'=>'Manage AhpCriteria', 'url'=>array('admin')),
);
?>

<h1>Ahp Criterias</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
