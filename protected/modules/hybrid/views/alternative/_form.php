<?php
/* @var $this AlternativeController */
/* @var $model AhpAlternative */
/* @var $form CActiveForm */
?>

<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-folder-open"></i> Set Alternative        </div>							
    </div>
    <div class="block-content collapse in">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ahp-alternative-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'htmlOptions' => array('class' => 'form-horizontal')
                ));
        ?>
        <?php echo $form->errorSummary($model, '', '', array('class' => 'alert alert-danger')); ?>
        <div class="form-body">
            <div class="control-group">
                <label class="control-label"><?php echo $form->labelEx($model, 'alternative_name'); ?>
                </label>                
                <div class="controls">
                    <?php echo $form->textField($model, 'alternative_name', array('size' => 50, 'maxlength' => 50)); ?>
                    <?php echo $form->error($model, 'alternative_name', array('class' => 'alert alert-danger')); ?>
                </div>                    
            </div>
            <div class="form-actions">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Simpan', array('class' => 'btn green')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->