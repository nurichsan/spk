<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-folder-open"></i> AhpCriteria        </div>							
    </div>
    <div class="block-content collapse in">        
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ahp-criteria-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'htmlOptions' => array('class' => 'form-horizontal')
        ));
        ?>
        <?php echo $form->errorSummary($model, '', '', array('class' => 'alert alert-danger')); ?>
        <div class="form-body">
            <div class="control-group">
                <label class="control-label"><?php echo $form->labelEx($model, 'category_id'); ?></label>                
                <div class="controls">
                    <?php echo $form->dropdownlist($model, 'category_id', CHtml::listData(AhpCategory::model()->findAll(), 'category_id', 'category_name'), array('empty' => 'Select')); ?>
                </div>                    
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo $form->labelEx($model, 'criteria_name'); ?></label>                
                <div class="controls">
                    <?php echo $form->textField($model, 'criteria_name', array('size' => 50, 'maxlength' => 50)); ?>
                </div>                    
            </div>
            <div class="form-actions">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Simpan' : 'Simpan', array('class' => 'btn green')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->