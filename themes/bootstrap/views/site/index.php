<style>
    table td{
        color: blue;
    }    
</style>
<div class="row-fluid">    
    <div class="span7">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">
                    <i class="icon-bullhorn"></i>                    
                </div>
            </div>
            <div class="block-content collapse in">                            
                <div class="row-fluid">
                    <h4>Info!!!</h4>
                    <p>
                    <ul>
                        <li>Dikembangkan menggunakan Apache, PHP, MySQL & Bootstrap Template.</li>
                        <li>Sesuaikan konfigurasi database pada <i><b>protected/config/main.php</b></i>.</li>                        
                        <li>Jika terjadi <span style="color:red;"><i>error</i></span> seperti berikut : 
                            <span style="color:red;"><i>CAssetManager.basePath "D:\xampp2\htdocs\spk\assets" is invalid. Please make sure the directory exists and is writable by the Web server process</i></span>. 
                            Solusi : buat folder dengan nama <span style="color:blue;">assets</span> dibawah root direktori aplikasi (setara dengan folder <span style="color:blue;">protected</span>).
                        </li>
                        <li>
                            Ubah permission pada direktori <span style="color:blue;">protected/runtime</span> dengan mode 0777 (khusus pengguna Unix).
                        </li>
                    </ul>
                    </p>
                </div>
            </div>
        </div>
        <div>
            <b>Febriani Sulistiyaningsih 15/388471/PPA/04911</b>
        </div>
    </div>
</div>