<?php

class DefaultController extends Controller {

    //public $layout = 'main';

    public function actionIndex() {
        /*
         * $prosen untuk menampung informasi bobot dan type dari suatu criteria
         */
        $mcri = SawCriteria::model()->findAll(array('order'=>'no_urut'));
        $prosen = array();
        foreach ($mcri as $row) {
            $prosen[$row->criteria_id]['prosen'] = $row->prosen;
            $prosen[$row->criteria_id]['type'] = $row->type;
            $prosen[$row->criteria_id]['max'] = 0;
            $prosen[$row->criteria_id]['min'] = 1000000;
        }
        $sql = "SELECT saw_value.person_id, person.person_name, saw_value.criteria_id, saw_criteria.criteria_name, saw_value.value FROM saw_value
                INNER JOIN person ON person.person_id=saw_value.person_id
                INNER JOIN saw_criteria ON saw_criteria.criteria_id=saw_value.criteria_id
                ORDER BY person.person_id, saw_criteria.no_urut";
        $query = Yii::app()->db->createCommand($sql)->queryAll();
        $xperson = array();
        $xcriteria = array();
        foreach ($query as $val) {
            $xperson[] = $val['person_name'];
            $xcriteria[] = $val['criteria_id'];
        }
        $person = array_unique($xperson);
        $criteria = array_unique($xcriteria);
        $data_value = array();
        $params = array();
        $i = 0;
        foreach ($person as $val1) {
            $data_value[$i]['person_name'] = $val1;
            foreach ($criteria as $val2) {
                foreach ($query as $val3) {
                    if ($val1 == $val3['person_name'] && $val2 == $val3['criteria_id']) {
                        $data_value[$i][$val2] = $val3['value'];
                    }
                }
            }
            $i++;
        }
        $this->render('index', array(
            'criteria' => $criteria,
            'person' => $person,
            'data_value' => $data_value,
            'prosen' => $prosen,
            'mcri' => $mcri
        ));
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}