<?php
/* @var $this CriteriaController */
/* @var $model AhpCriteria */

$this->breadcrumbs=array(
	'Ahp Criterias'=>array('index'),
	$model->criteria_id,
);

$this->menu=array(
array('label'=>'List AhpCriteria', 'url'=>array('index')),
array('label'=>'Create AhpCriteria', 'url'=>array('create')),
array('label'=>'Update AhpCriteria', 'url'=>array('update', 'id'=>$model->criteria_id)),
array('label'=>'Delete AhpCriteria', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->criteria_id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage AhpCriteria', 'url'=>array('admin')),
);
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-search"></i>
            View AhpCriteria #<?php echo $model->criteria_id; ?>        </div>
    </div>
    <div class="portlet-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'criteria_id',
		'criteria_name',
        ),
        )); ?>
    </div>
</div>