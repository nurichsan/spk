<?php
/* @var $this DefaultController */

$this->breadcrumbs = array(
    $this->module->id,
);
?>   
<div class="row-fluid">    
    <div class="span19">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">
                    <i class="icon-refresh"></i>
                    Value      
                </div>
            </div>
            <div class="block-content collapse in">            
                <table class="table table-bordered table-condensed table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Person</th>
                            <?php
                            foreach ($criteria as $xcri) {
                                echo "<th>" . $xcri . "</th>";
                            }
                            ?>
                        </tr>                    
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data_value as $val1) {
                            echo "<tr>";
                            echo "<td>" . $no . "</td>";
                            echo "<td>" . $val1['person_name'] . "</td>";
                            foreach ($criteria as $val2) {
                                if (array_key_exists($val2, $val1)) {
                                    echo "<td align='right'><span style='color:blue;'>" . $val1[$val2] . "</span></td>";
                                    if ($prosen[$val2]['type'] == 'benefit' && $val1[$val2] > $prosen[$val2]['max']) {
                                        $prosen[$val2]['max'] = $val1[$val2];
                                    }
                                    if ($prosen[$val2]['type'] == 'cost' && $val1[$val2] < $prosen[$val2]['min']) {
                                        $prosen[$val2]['min'] = $val1[$val2];
                                    }
                                }
                            }
                            echo "</td>";
                            $no++;
                        }
                        echo "<tr>";
                        echo "<td colspan=2>Total</td>";
                        foreach ($criteria as $val3) {
                            if ($prosen[$val3]['type'] == 'benefit') {
                                echo "<td align='right'><span style='color:red;font-weight:bold;'>" . $prosen[$val3]['max'] . "</span></td>";
                            } else {
                                echo "<td align='right'><span style='color:red;font-weight:bold;'>" . $prosen[$val3]['min'] . "</span></td>";
                            }
                        }
                        echo "</tr>";
                        ?>
                    </tbody>
                </table>                
            </div>
        </div>
    </div>    
</div>
<div class="row-fluid">    
    <div class="span19">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">
                    <i class="icon-refresh"></i>
                    Nomalisasi        
                </div>
            </div>
            <div class="block-content collapse in">            
                <table class="table table-bordered table-condensed table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Person</th>
                            <?php
                            foreach ($criteria as $xcri) {
                                echo "<th>" . $xcri . "</th>";
                            }
                            foreach ($mcri as $xcri) {
                                echo "<th>" . $xcri->criteria_id . ' (', $xcri->prosen . '%)' . "</th>";
                            }
                            ?>
                            <th>Total</th>
                        </tr>
                        <?php
                        $no = 1;
                        $result = array();
                        foreach ($data_value as $row1) {
                            echo "<tr>";
                            echo "<td>" . $no . "</td>";
                            echo "<td>" . $row1['person_name'] . "</td>";
                            foreach ($criteria as $row2) {
                                if (array_key_exists($row2, $row1)) {
                                    if ($prosen[$row2]['type'] == 'benefit') {
                                        $value = ($row1[$row2]) / $prosen[$row2]['max'];
                                    } else {
                                        $value = $prosen[$row2]['min'] / ($row1[$row2]);
                                    }
                                    echo "<td><span style='color:blue;'>" . number_format($value, 2, ',', '.') . "</span></td>";
                                }
                            }
                            $xtotal = 0;
                            foreach ($criteria as $row2) {
                                if (array_key_exists($row2, $row1)) {
                                    if ($prosen[$row2]['type'] == 'benefit') {
                                        $xvalue = (($row1[$row2]) / $prosen[$row2]['max']) * ($prosen[$row2]['prosen'] / 100);
                                    } else {
                                        $xvalue = ($prosen[$row2]['min'] / ($row1[$row2])) * ($prosen[$row2]['prosen'] / 100);
                                    }
                                    echo "<td><span style='color:blue;'>" . number_format($xvalue, 2, ',', '.') . "</span></td>";
                                    $xtotal += $xvalue;
                                }
                            }
                            echo "<td><span style='color:green;font-weight:bold;'>" . number_format($xtotal, 2, ',', '.') . "</span></td>";
                            echo "</tr>";
                            $result[$no - 1]['person_name'] = $row1['person_name'];
                            $result[$no - 1]['score'] = $xtotal;
                            $no++;
                        }
                        ?>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">    
    <div class="span6">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">
                    <i class="icon-refresh"></i>
                    Result        
                </div>
            </div>
            <div class="block-content collapse in">            
                <table class="table table-bordered table-condensed table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Person</th>
                            <th>Score</th>
                        </tr>                    
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($result as $key => $row) {
                            $person[$key] = $row['person_name'];
                            $score[$key] = $row['score'];
                        }
                        array_multisort($score, SORT_DESC, $result);
                        foreach ($result as $row) {
                            echo "<tr>";
                            echo "<td>" . $no . "</td>";
                            echo "<td>" . $row['person_name'] . "</td>";
                            echo "<td>" . number_format($row['score'], 2, ',', '.') . "</td>";
                            echo "</tr>";
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>                
            </div>
        </div>
    </div>    
</div>