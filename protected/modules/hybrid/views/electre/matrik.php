<div class="span12">
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">Electre Matrik</span>
            </div>							
        </div>
        <div class="block-content collapse in">            
            <div class="form-body">        
                <div style="color: blue; font-weight: blue; background-color: yellow; margin-bottom: 10px;" class="span4">
                    SUMSQ : Jumlah dari hasil pangkat masing-masing kolom
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Bobot</th>
                        <?php
                        foreach ($alternative as $alt) {
                            echo '<th>' . $alt->alternative_name . '</th>';
                        }
                        ?>
                        <th>SumSQ</th>
                    </tr>      
                    <?php
                    foreach ($all_criteria as $cri) {
                        $sumsq = 0;
                        echo '<tr>';
                        echo '<td>' . $cri->criteria_name . '</td>';
                        echo '<td>' . $matrik[$cri->criteria_id][0];
                        foreach ($alternative as $alt2) {
                            echo '<td>' . $matrik[$cri->criteria_id][$alt2->alternative_id] . '</td>';
                            $value = $matrik[$cri->criteria_id][$alt2->alternative_id];
                            $sumsq += ($value * $value);
                        }
                        echo '<td>' . $sumsq . '</td>';
                        echo '</tr>';
                        $matrik[$cri->criteria_id]['sumsq'] = $sumsq;
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">Normalisasi Decision Matriks</span>
            </div>							
        </div>
        <div class="block-content collapse in">            
            <div class="form-body">        
                <div style="color: blue; font-weight: blue; background-color: yellow; margin-bottom: 10px;" class="span4">
                    Nilai Alternative : Nilai Alternative / SQRT(SUMSQ)
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Bobot</th>
                        <?php
                        foreach ($alternative as $alt) {
                            echo '<th>' . $alt->alternative_name . '</th>';
                        }
                        ?>                        
                    </tr>      
                    <?php
                    foreach ($all_criteria as $cri) {
                        $sumsq = 0;
                        echo '<tr>';
                        echo '<td>' . $cri->criteria_name . '</td>';
                        echo '<td>' . $matrik[$cri->criteria_id][0];
                        foreach ($alternative as $alt2) {
                            $val1 = $matrik[$cri->criteria_id][$alt2->alternative_id];
                            $val_sumsq = $matrik[$cri->criteria_id]['sumsq'];
                            $val2 = $val1 / sqrt($val_sumsq);
                            echo '<td>' . $val2 . '</td>';
                        }
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>    
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">Weighted Normalized Decision Matriks</span>
            </div>							
        </div>
        <div class="block-content collapse in">            
            <div class="form-body">                
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>                        
                        <?php
                        foreach ($alternative as $alt) {
                            echo '<th>' . $alt->alternative_name . '</th>';
                        }
                        ?>                        
                    </tr>      
                    <?php
                    $normalisasi = array();
                    foreach ($all_criteria as $cri) {
                        $sumsq = 0;
                        echo '<tr>';
                        echo '<td>' . $cri->criteria_name . ' (<span style="color:red;">' . $cri->type . '</span>)' . '</td>';
                        foreach ($alternative as $alt2) {
                            $val1 = $matrik[$cri->criteria_id][$alt2->alternative_id];
                            $val_sumsq = $matrik[$cri->criteria_id]['sumsq'];
                            $val2 = $val1 / sqrt($val_sumsq);
                            $val3 = $val2 * $matrik[$cri->criteria_id][0];
                            echo '<td>' . $val3 . '</td>';
                            $normalisasi[$cri->criteria_id][$alt2->alternative_id] = $val3;
                        }
                        echo '</tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">Menentukan himpunan concordance (parameter yang lebih unggul) dan discordance (parameter yang tidak unggul)</span>
            </div>							
        </div>
        <div class="block-content collapse in">
            <div class="span3">
                <h4>Concordance</h4>                                  
                <?php
                $c = array();
                foreach ($alternative as $row) {
                    foreach ($alternative as $row2) {
                        if ($row->alternative_id == $row2->alternative_id)
                            continue;
                        echo 'C(' . $row->alternative_id . ', ' . $row2->alternative_id . ') : ';
                        foreach ($all_criteria as $row3) {
                            $label = '';
                            $value1 = $normalisasi[$row3->criteria_id][$row->alternative_id];
                            $value2 = $normalisasi[$row3->criteria_id][$row2->alternative_id];
                            if ($row3->type == 'cost') {
                                if ($value1 < $value2) {
                                    $label = $row3->criteria_id;
                                }
                            } else {
                                if ($value1 >= $value2) {
                                    $label = $row3->criteria_id;
                                }
                            }
                            echo $label . ' ';
                            if ($label == '')
                                continue;
                            $c[$row->alternative_id][$row2->alternative_id][] = $label;
                        }
                        echo '<br />';
                    }
                }
                ?>              
            </div>                                                                        
            <div class="span3">
                <h4>Discordance</h4>                                  
                <?php
                $d = array();
                foreach ($alternative as $row) {
                    foreach ($alternative as $row2) {
                        if ($row->alternative_id == $row2->alternative_id)
                            continue;
                        echo 'D(' . $row->alternative_id . ', ' . $row2->alternative_id . ') : ';
                        foreach ($all_criteria as $row3) {
                            $label = '';
                            $value1 = $normalisasi[$row3->criteria_id][$row->alternative_id];
                            $value2 = $normalisasi[$row3->criteria_id][$row2->alternative_id];
                            if ($row3->type == 'cost') {
                                if ($value1 > $value2) {
                                    $label = $row3->criteria_id;
                                }
                            } else {
                                if ($value1 < $value2) {
                                    $label = $row3->criteria_id;
                                }
                            }
                            echo $label . ' ';
                            if ($label == '')
                                continue;
                            $d[$row->alternative_id][$row2->alternative_id][] = $label;
                        }
                        echo '<br />';
                    }
                }
                ?>              
            </div> 
        </div>
    </div>
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">
                    Matrik Concordance
                </span>
            </div>							
        </div>
        <div class="block-content collapse in">
            <div class="span6">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <?php
                        foreach ($alternative as $alt) {
                            echo "<th>" . $alt->alternative_name . "</th>";
                        }
                        ?>
                    </tr>
                    <?php
                    $sum_all_matric_c = 0;
                    foreach ($alternative as $alt) {
                        echo "<tr>";
                        echo "<td>" . $alt->alternative_name . "</td>";
                        foreach ($alternative as $alt2) {
                            if ($alt->alternative_id === $alt2->alternative_id) {
                                echo "<td style='background-color:yellow;'></td>";
                            } else {
                                $val_c = 0;
                                foreach ($c[$alt->alternative_id][$alt2->alternative_id] as $value) {
                                    $val_c += $matrik[$value][0];
                                }
                                echo "<td>" . $val_c . "</td>";
                                $sum_all_matric_c += $val_c;
                            }
                        }
                        echo "</tr>";
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">
                    Matrik Discordance
                </span>
            </div>							
        </div>
        <div class="block-content collapse in">            
            <?php
            $dm = array();
            foreach ($alternative as $row) {
                foreach ($alternative as $row2) {
                    $max = 0;
                    if ($row->alternative_id === $row2->alternative_id)
                        continue;
                    echo "<table class='table table-bordered'>";
                    echo "<tr style='background-color:#CCFF99;'>";
                    echo "<th>#</th>";
                    foreach ($d[$row->alternative_id][$row2->alternative_id] as $key) {
                        echo "<th>" . $key . "</th>";
                    }
                    echo "<th>Max</th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td><span style='color:red;font-weight:bold;'>D(" . $row->alternative_id . "," . $row2->alternative_id . ")" . "</span></td>";
                    foreach ($d[$row->alternative_id][$row2->alternative_id] as $key) {
                        $val1 = $normalisasi[$key][$row->alternative_id];
                        $val2 = $normalisasi[$key][$row2->alternative_id];
                        $val3 = abs(($val1 - $val2));
                        echo "<td>" . $val3 . "</td>";
                        if ($val3 > $max) {
                            $max = $val3;
                        }
                    }
                    echo "<td>" . $max . "</td>";
                    echo "</tr>";
                    echo "</table>";
                    $max2 = 0;
                    echo "<table class='table table-bordered'>";
                    echo "<tr style='background-color:#FF99CC'>";
                    echo "<th>Criteria</th>";
                    foreach ($all_criteria as $cri) {
                        echo "<th>" . $cri->criteria_id . "</td>";
                    }
                    echo "<th>Max</th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<td><span style='color:blue;font-weight:bold;'>D(" . $row->alternative_id . "," . $row2->alternative_id . ")" . "</span></td>";
                    foreach ($all_criteria as $cri) {
                        $val1 = $normalisasi[$cri->criteria_id][$row->alternative_id];
                        $val2 = $normalisasi[$cri->criteria_id][$row2->alternative_id];
                        $val3 = abs(($val1 - $val2));
                        echo "<td>" . $val3 . "</td>";
                        if ($val3 > $max2) {
                            $max2 = $val3;
                        }
                    }
                    echo "<td>" . $max2 . "</td>";
                    echo "</tr>";
                    echo "</table>";
                    $dm[$row->alternative_id][$row2->alternative_id] = $max / $max2;
                }
            }
            ?>   
            <table class="table table-bordered">
                <tr>
                    <th>#</th>
                    <?php
                    foreach ($alternative as $alt) {
                        echo "<th>" . $alt->alternative_name . "</th>";
                    }
                    ?>
                </tr>
                <?php
                $sum_all_matric_d = 0;
                foreach ($alternative as $alt) {
                    echo "<tr>";
                    echo "<td>" . $alt->alternative_name . "</td>";
                    foreach ($alternative as $alt2) {
                        if ($alt->alternative_id === $alt2->alternative_id) {
                            echo "<td style='background-color:yellow;'></td>";
                        } else {
                            $val_dm = $dm[$alt->alternative_id][$alt2->alternative_id];
                            echo "<td>" . $val_dm . "</td>";
                            $sum_all_matric_d += $val_dm;
                        }
                    }
                    echo "</tr>";
                }
                ?>
            </table>
        </div>
    </div>
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">
                    Domain Matrik Concordance & Discordance
                </span>
            </div>							
        </div>
        <div class="block-content collapse in">
            <div class="span6">
                <h4>Domain Matrik C</h4>
                <div style="background-color: yellow; color: blue; width: 40%; margin-bottom: 10px; font-weight: bold;">
                    Jumlah Matrik C = <?php echo $sum_all_matric_c; ?> <br />
                    Jumlah Kombinasi = <?php echo Combination::combinations(count($alternative), 2); ?> <br />
                    Nilai C (garis atas) = <?php echo $top_c = ($sum_all_matric_c / Combination::combinations(count($alternative), 2)); ?>
                </div>
                <table class = "table table-bordered">
                    <tr>
                        <th>#</th>
                        <?php
                        foreach ($alternative as $alt) {
                            echo "<th>" . $alt->alternative_name . "</th>";
                        }
                        ?>
                    </tr>
                    <?php
                    $domain_c = array();
                    foreach ($alternative as $alt) {
                        echo "<tr>";
                        echo "<td>" . $alt->alternative_name . "</td>";
                        foreach ($alternative as $alt2) {
                            if ($alt->alternative_id === $alt2->alternative_id) {
                                echo "<td style='background-color:yellow;'></td>";
                            } else {
                                $val_c = 0;
                                foreach ($c[$alt->alternative_id][$alt2->alternative_id] as $value) {
                                    $val_c += $matrik[$value][0];
                                }
                                if ($val_c >= $top_c) {
                                    $result = 1;
                                    echo "<td><span style='color:blue; font-weight:bold;'>" . $result . "</span></td>";
                                } else {
                                    $result = 0;
                                    echo "<td>" . $result . "</td>";
                                }
                                $domain_c[$alt->alternative_id][$alt2->alternative_id] = $result;
                            }
                        }
                        echo "</tr>";
                    }
                    ?>
                </table>
            </div>
            <div class="span6">
                <h4>Domain Matrik D</h4>
                <div style="background-color: yellow; color: blue; width: 40%; margin-bottom: 10px; font-weight: bold;">
                    Jumlah Matrik D = <?php echo $sum_all_matric_d; ?> <br />
                    Jumlah Kombinasi = <?php echo Combination::combinations(count($alternative), 2); ?> <br />
                    Nilai D (garis atas) = <?php echo $top_d = ($sum_all_matric_d / Combination::combinations(count($alternative), 2)); ?>
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <?php
                        foreach ($alternative as $alt) {
                            echo "<th>" . $alt->alternative_name . "</th>";
                        }
                        ?>
                    </tr>
                    <?php
                    $domain_d = array();
                    foreach ($alternative as $alt) {
                        echo "<tr>";
                        echo "<td>" . $alt->alternative_name . "</td>";
                        foreach ($alternative as $alt2) {
                            if ($alt->alternative_id === $alt2->alternative_id) {
                                echo "<td style='background-color:yellow;'></td>";
                            } else {
                                $val_dm = $dm[$alt->alternative_id][$alt2->alternative_id];
                                if ($val_dm <= $top_d) {
                                    $result = 1;
                                    echo "<td><span style='color:blue; font-weight:bold;'>" . $result . "</span></td>";
                                } else {
                                    $result = 0;
                                    echo "<td>" . $result . "</td>";
                                }
                                $domain_d[$alt->alternative_id][$alt2->alternative_id] = $result;
                            }
                        }
                        echo "</tr>";
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
    <div class="block">   
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <i class="icon-list"></i> <span style="color:blue;font-weight: bold;">
                    Agregat Dominan Matriks
                </span>
            </div>							
        </div>
        <div class="block-content collapse in">            
            <div class="span6">
                <h4>Final Result</h4>                
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <?php
                        foreach ($alternative as $alt) {
                            echo "<th>" . $alt->alternative_name . "</th>";
                        }
                        ?>
                    </tr>
                    <?php
                    foreach ($alternative as $alt) {
                        echo "<tr>";
                        echo "<td>" . $alt->alternative_name . "</td>";
                        foreach ($alternative as $alt2) {
                            if ($alt->alternative_id === $alt2->alternative_id) {
                                echo "<td style='background-color:yellow;'></td>";
                            } else {
                                $final = $domain_c[$alt->alternative_id][$alt2->alternative_id] * $domain_d[$alt->alternative_id][$alt2->alternative_id];
                                echo "<td>" . $final . "</td>";
                            }
                        }
                        echo "</tr>";
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>