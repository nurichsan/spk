<?php

/**
 * This is the model class for table "ahp_criteria".
 *
 * The followings are the available columns in table 'ahp_criteria':
 * @property integer $criteria_id
 * @property string $criteria_name
 */
class AhpCriteria extends CActiveRecord {

    public $category_name;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'ahp_criteria';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('category_id, criteria_name', 'required'),
            array('criteria_name', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('criteria_id, criteria_name, category_name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rel_category' => array(self::BELONGS_TO, 'AhpCategory', 'category_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'criteria_id' => 'Criteria ID',
            'criteria_name' => 'Criteria Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('criteria_id', $this->criteria_id);
        $criteria->compare('criteria_name', $this->criteria_name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AhpCriteria the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
