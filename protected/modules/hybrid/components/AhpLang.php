<?php

/**
 * Description of AhpLang
 *
 * @author Nur Ichsan
 */
class AhpLang {

    public static function type1() {
        return array(
            2 => 'Kecil',
            4 => 'Sedang',
            6 => 'Besar'
        );
    }

    public static function type2() {
        return array(
            2 => 'Lama',
            4 => 'Baru'
        );
    }

}

?>
