<?php

class CriteriaController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'setvalue', 'pairwise'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionPairwise() {
        $category = AhpCategory::model()->findAll();
        $m_pairwise = AhpPairWise::model()->findAll();
        //convert to array       
        $pair_wise = array();
        foreach ($category as $rowcat) {
            $m_pairwise = AhpPairWise::model()->findAllByAttributes(array('category_id' => $rowcat->category_id));
            foreach ($m_pairwise as $row) {
                $pair_wise[$rowcat->category_id][$row->criteria_1][$row->criteria_2] = $row->value;
            }
        }
        $this->render('pairwise', array(
            'pair_wise' => $pair_wise,
            'category' => $category
        ));
    }

    public function getValue($id) {
        $data = array();
        $value = AhpPairWise::model()->findAllByAttributes(array('criteria_1' => $id));
        if ($value !== NULL)
            foreach ($value as $row) {
                $data[$row->criteria_2] = $row->value;
            }
        return $data;
    }

    public function actionSetvalue($id, $cat) {
        $model = AhpCriteria::model()->findByPk($id);
        $criteria = AhpCriteria::model()->findAll('criteria_id <> :id and category_id = :param', array(':id' => $id, ':param' => $cat));
        $all_criteria = AhpCriteria::model()->findAllByAttributes(array('category_id' => $cat));
        $data = $this->getValue($id);
        if (isset($_POST['Value'])) {
            $del_criteria = new CDbCriteria();
            $del_criteria->condition = "(criteria_1 = :param1 OR criteria_2 = :param2) AND category_id = :param3 ";
            $del_criteria->params = array(
                ':param1' => $id, ':param2' => $id, ':param3' => $cat
            );
            AhpPairWise::model()->deleteAll($del_criteria);
            $same = new AhpPairWise();
            $same->criteria_1 = $id;
            $same->criteria_2 = $id;
            $same->category_id = $cat;
            $same->value = 1;
            $same->save(false);
            foreach ($criteria as $row) {
                if (isset($_POST['Value'][$row->criteria_id])) {
                    $insert = new AhpPairWise();
                    $insert->criteria_1 = $id;
                    $insert->criteria_2 = $row->criteria_id;
                    $insert->category_id = $cat;
                    $insert->value = $_POST['Value'][$row->criteria_id];
                    $insert->save(false);
                    $deff = new AhpPairWise();
                    $deff->criteria_1 = $row->criteria_id;
                    $deff->criteria_2 = $id;
                    $deff->category_id = $cat;
                    $deff->value = 1 / $_POST['Value'][$row->criteria_id];
                    $deff->save(false);
                }
            }
            $this->redirect(array('setvalue', 'id' => $id, 'cat' => $cat));
        }
        $m_pairwise = AhpPairWise::model()->findAllByAttributes(array('category_id' => $cat));
        //convert to array       
        $pair_wise = array();
        foreach ($m_pairwise as $row) {
            $pair_wise[$row->criteria_1][$row->criteria_2] = $row->value;
        }
        $this->render('setvalue', array(
            'model' => $model,
            'criteria' => $criteria,
            'data' => $data,
            'pair_wise' => $pair_wise,
            'all_criteria' => $all_criteria
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new AhpCriteria;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AhpCriteria'])) {
            $model->attributes = $_POST['AhpCriteria'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->criteria_id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['AhpCriteria'])) {
            $model->attributes = $_POST['AhpCriteria'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->criteria_id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('AhpCriteria');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new AhpCriteria('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['AhpCriteria']))
            $model->attributes = $_GET['AhpCriteria'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return AhpCriteria the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = AhpCriteria::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param AhpCriteria $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ahp-criteria-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
