<?php
/* @var $this AlternativeController */
/* @var $model AhpAlternative */

$this->breadcrumbs=array(
	'Ahp Alternatives'=>array('index'),
	$model->alternative_id,
);

$this->menu=array(
array('label'=>'List AhpAlternative', 'url'=>array('index')),
array('label'=>'Create AhpAlternative', 'url'=>array('create')),
array('label'=>'Update AhpAlternative', 'url'=>array('update', 'id'=>$model->alternative_id)),
array('label'=>'Delete AhpAlternative', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->alternative_id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage AhpAlternative', 'url'=>array('admin')),
);
?>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-search"></i>
            View AhpAlternative #<?php echo $model->alternative_id; ?>        </div>
    </div>
    <div class="portlet-body">
        <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
        		'alternative_id',
		'alternative_name',
        ),
        )); ?>
    </div>
</div>