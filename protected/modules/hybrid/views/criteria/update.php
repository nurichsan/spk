<?php
/* @var $this CriteriaController */
/* @var $model AhpCriteria */

$this->breadcrumbs=array(
	'Ahp Criterias'=>array('index'),
	$model->criteria_id=>array('view','id'=>$model->criteria_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AhpCriteria', 'url'=>array('index')),
	array('label'=>'Create AhpCriteria', 'url'=>array('create')),
	array('label'=>'View AhpCriteria', 'url'=>array('view', 'id'=>$model->criteria_id)),
	array('label'=>'Manage AhpCriteria', 'url'=>array('admin')),
);
?>

<h1>Update AhpCriteria <?php echo $model->criteria_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>