<?php
/* @var $this PersonController */
/* @var $model Person */
/* @var $form CActiveForm */
?>

<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-user"></i> <span style="color:blue;font-weight: bold;">Person : <?php echo $person->person_id . ' / ' . $person->person_name; ?></span>
        </div>							
    </div>
    <div class="block-content collapse in">
        <?php echo CHtml::form('', 'POST', array('class' => 'form-horizontal')); ?>
        <div class="form-body">
            <?php foreach ($criteria as $row) : ?>                
                <div class="control-group">
                    <label class="control-label"><?php echo $row->criteria_name; ?></label>                
                    <div class="controls">
                        <?php echo CHtml::textField('Value[' . $row->criteria_id . ']', (isset($data[$row->criteria_id]) ? $data[$row->criteria_id] : ''), array('size' => 5, 'maxlength' => 5, 'data-required' => '1', 'class' => 'span6 m-wrap')); ?><span style="color: red;"> <?php echo $row->type; ?></span>
                    </div>                    
                </div>
            <?php endforeach; ?>
            <div class="form-actions">
                <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div><!-- form -->