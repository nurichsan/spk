<?php
/* @var $this CriteriaController */
/* @var $model AhpCriteria */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'criteria_id'); ?>
		<?php echo $form->textField($model,'criteria_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'criteria_name'); ?>
		<?php echo $form->textField($model,'criteria_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->