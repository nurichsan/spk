<?php

/**
 * Description of Electre
 *
 * @author Nur Ichsan
 */
class Electre {

    public static function getMatrik($all_criteria) {
        /*
         * return array(
         *  'bobot', 'jarak', dll
         * )
         */
        $result = array();
        $pair_wise = PairWiseCalculation::getData($all_criteria);
        $bobot = self::getBobot($pair_wise);

        $ahp_value = self::ahpValue();
        $matrik_ahp = array();
        $i = 0;
        foreach ($all_criteria as $row) {
            $result[$row->criteria_id][0] = $bobot[$i];
            $i++;
        }        
        foreach ($all_criteria as $row) {
            foreach ($ahp_value as $val) {
                if ($row->criteria_id == $val->criteria_id) {
                    $result[$row->criteria_id][$val->alternative_id] = $val->value;
                }
            }
        }        
        return $result;
    }

    public static function getBobot($pair_wise) {
        $bobot = array();
        for ($i = 0; $i < count($pair_wise); $i++) {
            $bobot[$i] = $pair_wise[$i][count($pair_wise[$i]) - 1]; //bobot            
        }
        return $bobot;
    }

    public static function ahpValue() {
        return AhpValue::model()->findAll();
    }

}

?>
