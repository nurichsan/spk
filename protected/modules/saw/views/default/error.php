<div class="alert alert-error alert-block">
    <h4 class="alert-heading"><?php echo $code; ?></h4>
    <?php echo CHtml::encode($message); ?>
</div>