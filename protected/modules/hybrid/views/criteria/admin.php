<?php
/* @var $this CriteriaController */
/* @var $model AhpCriteria */

$this->breadcrumbs = array(
    'Ahp Criterias' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List AhpCriteria', 'url' => array('index')),
    array('label' => 'Create AhpCriteria', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#ahp-criteria-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>
<div class="alert alert-info">
    <h4>Info!! </h4>               
    Jika menambah kriteria atau <i>alternative</i> baru. Pastikan setiap <i>alternative</i> juga memiliki nilai terhadap kriteria.
</div>
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">
            <i class="icon-list"></i>
            Manage Ahp Criteria
        </div>
    </div>
    <div class="block-content collapse in">        
        <div class="span12">
            <?php echo CHtml::link(CHtml::htmlButton('<i class="icon icon-folder-open"></i> Create', array('class' => 'btn btn-success')), Yii::app()->controller->createUrl("create")); ?>            
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'ahp-criteria-grid',
                'dataProvider' => $model->search(),
                //'filter' => $model,
                'itemsCssClass' => 'table table-striped table-bordered',
                'columns' => array(
                    'criteria_id',
                    array(
                        'name' => 'category_name',
                        'value' => '$data->rel_category->category_name'
                    ),
                    'criteria_name',
                    array(
                        'type' => 'raw',
                        'value' => 'CHtml::link("Set Value", Yii::app()->controller->createUrl("setvalue", array("id"=>$data->criteria_id, "cat"=>$data->category_id)))'
                    ),
                    array(
                        'class' => 'CButtonColumn',
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</div>