<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MyButtonColumn
 * Fungsi dari MyButtonColumn mengganti Class CButtonColumn pada CGridView. 
 * array('class'=>'MyButtonColumn')
 * 
 * Semua attribute dan properti dari CButtonColumn dapat digunakan oleh MyButtonColumn
 *
 * @author Nur Ichsan
 */
class MyButtonColumn extends CButtonColumn {

    public $evaluateID = true;

    public function init() {
        $this->viewButtonLabel = "<i class='icon-check'></i>";
        $this->viewButtonImageUrl = FALSE;
        $this->viewButtonOptions = array(
            'class' => 'btn btn-success btn-condensed btn-sm',
            'title' => 'Detail'
        );
        $this->updateButtonLabel = "<i class='icon-pencil'></i>";
        $this->updateButtonOptions = array(
            'class' => 'btn btn-info btn-sm btn-condensed',
            'title' => 'Update'
        );
        $this->updateButtonImageUrl = FALSE;
        $this->deleteButtonLabel = "<i class='icon-trash'></i>";
        $this->deleteButtonOptions = array(
            'class' => 'btn btn-danger btn-sm btn-condensed',
            'title' => 'Hapus'
        );
        $this->deleteButtonImageUrl = FALSE;
        $this->htmlOptions = array(
            'style' => 'width:100px',
        );
        return parent::init();
    }

    public function renderDataCellContent($row, $data) {
        $tr = array();
        ob_start();
        foreach ($this->buttons as $id => $button) {
            if ($this->evaluateID and isset($button['options']['id'])) {
                $button['options']['id'] = $this->evaluateExpression($button['options']['id'], array('row' => $row, 'data' => $data));
            }

            $this->renderButton($id, $button, $row, $data);
            $tr['{' . $id . '}'] = ob_get_contents();
            ob_clean();
        }
        ob_end_clean();
        echo strtr($this->template, $tr);
    }

}

?>
