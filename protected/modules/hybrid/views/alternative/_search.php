<?php
/* @var $this AlternativeController */
/* @var $model AhpAlternative */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'alternative_id'); ?>
		<?php echo $form->textField($model,'alternative_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alternative_name'); ?>
		<?php echo $form->textField($model,'alternative_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->