<?php
/* @var $this CriteriaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Saw Criterias',
);

$this->menu=array(
	array('label'=>'Create SawCriteria', 'url'=>array('create')),
	array('label'=>'Manage SawCriteria', 'url'=>array('admin')),
);
?>

<h1>Saw Criterias</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
