-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: spk
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ahp_alternative`
--

DROP TABLE IF EXISTS `ahp_alternative`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahp_alternative` (
  `alternative_id` int(11) NOT NULL AUTO_INCREMENT,
  `alternative_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`alternative_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahp_alternative`
--

LOCK TABLES `ahp_alternative` WRITE;
/*!40000 ALTER TABLE `ahp_alternative` DISABLE KEYS */;
/*!40000 ALTER TABLE `ahp_alternative` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ahp_category`
--

DROP TABLE IF EXISTS `ahp_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahp_category` (
  `category_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahp_category`
--

LOCK TABLES `ahp_category` WRITE;
/*!40000 ALTER TABLE `ahp_category` DISABLE KEYS */;
INSERT INTO `ahp_category` VALUES (1,'Umum'),(2,'Ruangan'),(3,'Halaman');
/*!40000 ALTER TABLE `ahp_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ahp_consistency_index`
--

DROP TABLE IF EXISTS `ahp_consistency_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahp_consistency_index` (
  `item` int(11) NOT NULL DEFAULT '0',
  `value` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahp_consistency_index`
--

LOCK TABLES `ahp_consistency_index` WRITE;
/*!40000 ALTER TABLE `ahp_consistency_index` DISABLE KEYS */;
INSERT INTO `ahp_consistency_index` VALUES (1,0.00),(2,0.00),(3,0.58),(4,0.90),(5,1.12),(6,1.24),(7,1.32),(8,1.41),(9,1.45),(10,1.49),(11,1.51),(12,1.48),(13,1.56),(14,1.57),(15,1.59);
/*!40000 ALTER TABLE `ahp_consistency_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ahp_criteria`
--

DROP TABLE IF EXISTS `ahp_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahp_criteria` (
  `criteria_id` int(11) NOT NULL AUTO_INCREMENT,
  `criteria_name` varchar(50) DEFAULT NULL,
  `label` varchar(3) DEFAULT NULL,
  `type_field` enum('dropdown','textfield') DEFAULT 'textfield',
  `type` enum('benefit','cost') DEFAULT 'benefit',
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`criteria_id`),
  KEY `PCategory` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahp_criteria`
--

LOCK TABLES `ahp_criteria` WRITE;
/*!40000 ALTER TABLE `ahp_criteria` DISABLE KEYS */;
INSERT INTO `ahp_criteria` VALUES (1,'Kesetiaan','U1','textfield','benefit',1),(2,'Prestasi Kerja','U2','textfield','benefit',1),(3,'Tanggung Jawab','U3','dropdown','benefit',1),(4,'Ketaatan','U4','dropdown','benefit',1),(5,'Kejujuran','U5','textfield','benefit',1),(6,'Kerjasama','U6','dropdown','benefit',1),(7,'Loyalitas','U7','textfield','benefit',1),(8,'Kedisplinan','U8','textfield','benefit',1),(9,'Hubungan Antar Manusia','U9','textfield','benefit',1),(10,'Kreatifitas','U10','textfield','benefit',1),(11,'Prestasi','U11','textfield','benefit',1),(12,'Mengeluarkan Sampah','R1','textfield','benefit',2),(13,'Membersihkan Tempat Sampah','R2','textfield','benefit',2),(14,'Membersihkan Furniture','R3','textfield','benefit',2),(15,'Menyapu dan Mengepel Lantai','R4','textfield','benefit',2),(16,'Membersihkan KM, WC dan Watafel','R5','textfield','benefit',2),(17,'Membersihkan Pintu, Kaca dan Jendela','R6','textfield','benefit',2),(18,'Membersihkan Dindin, Ventilasi dan Langit-Langit','R7','textfield','benefit',2),(19,'Pengelolaan Peralatan Kerja','R8','textfield','benefit',2),(20,'Menyapu','H1','textfield','benefit',3),(21,'Membersihkan Saluran Hujan','H2','textfield','benefit',3),(22,'Membuang Sampah ke TPS','H3','textfield','benefit',3),(23,'Mencuci Tampat Sampah','H4','textfield','benefit',3),(24,'Menguras Kolam Ikan','H5','textfield','benefit',3),(25,'Merapihkan Taman','H6','textfield','benefit',3),(26,'Kerja Bakti','H7','textfield','benefit',3),(27,'Pengelolaan Peralatan Kerja','H8','textfield','benefit',3);
/*!40000 ALTER TABLE `ahp_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ahp_pair_wise`
--

DROP TABLE IF EXISTS `ahp_pair_wise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahp_pair_wise` (
  `category_id` int(11) NOT NULL,
  `criteria_1` int(11) NOT NULL DEFAULT '0',
  `criteria_2` int(11) NOT NULL DEFAULT '0',
  `value` decimal(14,3) DEFAULT NULL,
  PRIMARY KEY (`criteria_1`,`criteria_2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahp_pair_wise`
--

LOCK TABLES `ahp_pair_wise` WRITE;
/*!40000 ALTER TABLE `ahp_pair_wise` DISABLE KEYS */;
INSERT INTO `ahp_pair_wise` VALUES (1,1,1,1.000),(1,1,2,3.003),(1,1,3,2.000),(1,1,4,2.000),(1,1,5,3.003),(1,1,6,2.000),(1,1,7,2.000),(1,1,8,1.000),(1,1,9,2.000),(1,1,10,2.000),(1,1,11,3.000),(1,2,1,0.333),(1,2,2,1.000),(1,2,3,3.003),(1,2,4,2.000),(1,2,5,2.000),(1,2,6,1.000),(1,2,7,2.000),(1,2,8,2.000),(1,2,9,3.003),(1,2,10,2.000),(1,2,11,2.000),(1,3,1,0.500),(1,3,2,0.333),(1,3,3,1.000),(1,3,4,2.000),(1,3,5,2.000),(1,3,6,3.003),(1,3,7,2.000),(1,3,8,2.000),(1,3,9,2.000),(1,3,10,3.003),(1,3,11,2.000),(1,4,1,0.500),(1,4,2,0.500),(1,4,3,0.500),(1,4,4,1.000),(1,4,5,3.003),(1,4,6,2.000),(1,4,7,3.003),(1,4,8,3.003),(1,4,9,2.000),(1,4,10,2.000),(1,4,11,3.000),(1,5,1,0.333),(1,5,2,0.500),(1,5,3,0.500),(1,5,4,0.333),(1,5,5,1.000),(1,5,6,2.000),(1,5,7,2.000),(1,5,8,2.000),(1,5,9,2.000),(1,5,10,2.000),(1,5,11,2.000),(1,6,1,0.500),(1,6,2,1.000),(1,6,3,0.333),(1,6,4,0.500),(1,6,5,0.500),(1,6,6,1.000),(1,6,7,2.000),(1,6,8,3.003),(1,6,9,1.000),(1,6,10,2.000),(1,6,11,2.000),(1,7,1,0.500),(1,7,2,0.500),(1,7,3,0.500),(1,7,4,0.333),(1,7,5,0.500),(1,7,6,0.500),(1,7,7,1.000),(1,7,8,2.000),(1,7,9,2.000),(1,7,10,3.003),(1,7,11,3.000),(1,8,1,1.000),(1,8,2,0.500),(1,8,3,0.500),(1,8,4,0.333),(1,8,5,0.500),(1,8,6,0.333),(1,8,7,0.500),(1,8,8,1.000),(1,8,9,3.003),(1,8,10,2.000),(1,8,11,2.000),(1,9,1,0.500),(1,9,2,0.333),(1,9,3,0.500),(1,9,4,0.500),(1,9,5,0.500),(1,9,6,1.000),(1,9,7,0.500),(1,9,8,0.333),(1,9,9,1.000),(1,9,10,2.000),(1,9,11,2.000),(1,10,1,0.500),(1,10,2,0.500),(1,10,3,0.333),(1,10,4,0.500),(1,10,5,0.500),(1,10,6,0.500),(1,10,7,0.333),(1,10,8,0.500),(1,10,9,0.500),(1,10,10,1.000),(1,10,11,3.000),(1,11,1,0.333),(1,11,2,0.500),(1,11,3,0.500),(1,11,4,0.333),(1,11,5,0.500),(1,11,6,0.500),(1,11,7,0.333),(1,11,8,0.500),(1,11,9,0.500),(1,11,10,0.333),(1,11,11,1.000),(2,12,12,1.000),(2,12,13,2.000),(2,12,14,2.000),(2,12,15,2.000),(2,12,16,1.000),(2,12,17,2.000),(2,12,18,2.000),(2,12,19,2.000),(2,13,12,0.500),(2,13,13,1.000),(2,13,14,2.000),(2,13,15,1.000),(2,13,16,2.000),(2,13,17,2.000),(2,13,18,1.000),(2,13,19,1.000),(2,14,12,0.500),(2,14,13,0.500),(2,14,14,1.000),(2,14,15,3.003),(2,14,16,1.000),(2,14,17,2.000),(2,14,18,2.000),(2,14,19,2.000),(2,15,12,0.500),(2,15,13,1.000),(2,15,14,0.333),(2,15,15,1.000),(2,15,16,2.000),(2,15,17,1.000),(2,15,18,1.000),(2,15,19,1.000),(2,16,12,1.000),(2,16,13,0.500),(2,16,14,1.000),(2,16,15,0.500),(2,16,16,1.000),(2,16,17,3.003),(2,16,18,2.000),(2,16,19,1.000),(2,17,12,0.500),(2,17,13,0.500),(2,17,14,0.500),(2,17,15,1.000),(2,17,16,0.333),(2,17,17,1.000),(2,17,18,2.000),(2,17,19,2.000),(2,18,12,0.500),(2,18,13,1.000),(2,18,14,0.500),(2,18,15,1.000),(2,18,16,0.500),(2,18,17,0.500),(2,18,18,1.000),(2,18,19,3.000),(2,19,12,0.500),(2,19,13,1.000),(2,19,14,0.500),(2,19,15,1.000),(2,19,16,1.000),(2,19,17,0.500),(2,19,18,0.333),(2,19,19,1.000),(3,20,20,1.000),(3,20,21,2.000),(3,20,22,3.003),(3,20,23,4.000),(3,20,24,3.003),(3,20,25,2.000),(3,20,26,5.000),(3,20,27,5.988),(3,21,20,0.500),(3,21,21,1.000),(3,21,22,2.000),(3,21,23,3.003),(3,21,24,3.003),(3,21,25,4.000),(3,21,26,2.000),(3,21,27,4.000),(3,22,20,0.333),(3,22,21,0.500),(3,22,22,1.000),(3,22,23,2.000),(3,22,24,2.000),(3,22,25,3.003),(3,22,26,5.000),(3,22,27,2.000),(3,23,20,0.250),(3,23,21,0.333),(3,23,22,0.500),(3,23,23,1.000),(3,23,24,4.000),(3,23,25,3.003),(3,23,26,3.003),(3,23,27,2.000),(3,24,20,0.333),(3,24,21,0.333),(3,24,22,0.500),(3,24,23,0.250),(3,24,24,1.000),(3,24,25,5.000),(3,24,26,4.000),(3,24,27,3.003),(3,25,20,0.500),(3,25,21,0.250),(3,25,22,0.333),(3,25,23,0.333),(3,25,24,0.200),(3,25,25,1.000),(3,25,26,2.000),(3,25,27,1.000),(3,26,20,0.200),(3,26,21,0.500),(3,26,22,0.200),(3,26,23,0.333),(3,26,24,0.250),(3,26,25,0.500),(3,26,26,1.000),(3,26,27,3.003),(3,27,20,0.167),(3,27,21,0.250),(3,27,22,0.500),(3,27,23,0.500),(3,27,24,0.333),(3,27,25,1.000),(3,27,26,0.333),(3,27,27,1.000);
/*!40000 ALTER TABLE `ahp_pair_wise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ahp_value`
--

DROP TABLE IF EXISTS `ahp_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ahp_value` (
  `alternative_id` int(11) NOT NULL DEFAULT '0',
  `criteria_id` int(11) NOT NULL DEFAULT '0',
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`alternative_id`,`criteria_id`),
  KEY `criteria_id` (`criteria_id`),
  CONSTRAINT `ahp_value_ibfk_1` FOREIGN KEY (`alternative_id`) REFERENCES `ahp_alternative` (`alternative_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ahp_value_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `ahp_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ahp_value`
--

LOCK TABLES `ahp_value` WRITE;
/*!40000 ALTER TABLE `ahp_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ahp_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `person_id` varchar(5) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES ('PS001','RONALDO'),('PS002','RAMOS'),('PS003','JAMES'),('PS004','BENZEMA'),('PS005','BALE'),('PS006','VARANE'),('PS007','MARCELLO'),('PS008','CARVAJAL'),('PS009','KROOS'),('PS010','MODRIC'),('PS011','NAVAS'),('PS012','CASEMIRO'),('PS013','DANILO'),('PS014','ARBELOA'),('PS015','NACHO'),('PS016','KOVACIC');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saw_criteria`
--

DROP TABLE IF EXISTS `saw_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saw_criteria` (
  `criteria_id` varchar(5) NOT NULL DEFAULT '',
  `criteria_name` varchar(100) DEFAULT NULL,
  `prosen` decimal(14,2) DEFAULT NULL,
  `type` enum('benefit','cost','','') NOT NULL DEFAULT 'benefit',
  `no_urut` int(11) DEFAULT NULL,
  PRIMARY KEY (`criteria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saw_criteria`
--

LOCK TABLES `saw_criteria` WRITE;
/*!40000 ALTER TABLE `saw_criteria` DISABLE KEYS */;
INSERT INTO `saw_criteria` VALUES ('C1','KELINCAHAN',9.00,'benefit',1),('C10','KOGNITIF',9.00,'benefit',10),('C11','EMOSIONAL',5.00,'cost',11),('C2','KESEIMBANGAN',10.00,'benefit',2),('C3','KECEPATAN REAKSI',11.00,'benefit',3),('C4','PASSING BAWAH',12.00,'benefit',4),('C5','PASSING ATAS',11.00,'benefit',5),('C6','DRIBBLING',8.00,'benefit',6),('C7','SHOOTING',10.00,'benefit',7),('C8','HEADING',8.00,'benefit',8),('C9','TACKLING',7.00,'cost',9);
/*!40000 ALTER TABLE `saw_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saw_value`
--

DROP TABLE IF EXISTS `saw_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saw_value` (
  `person_id` varchar(5) NOT NULL,
  `criteria_id` varchar(5) NOT NULL,
  `value` decimal(14,2) NOT NULL,
  PRIMARY KEY (`person_id`,`criteria_id`),
  KEY `criteria_id` (`criteria_id`),
  CONSTRAINT `saw_value_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `saw_value_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `saw_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saw_value`
--

LOCK TABLES `saw_value` WRITE;
/*!40000 ALTER TABLE `saw_value` DISABLE KEYS */;
INSERT INTO `saw_value` VALUES ('PS001','C1',90.00),('PS001','C10',95.00),('PS001','C11',10.00),('PS001','C2',85.00),('PS001','C3',90.00),('PS001','C4',70.00),('PS001','C5',80.00),('PS001','C6',90.00),('PS001','C7',96.00),('PS001','C8',90.00),('PS001','C9',60.00),('PS002','C1',75.00),('PS002','C10',80.00),('PS002','C11',70.00),('PS002','C2',89.00),('PS002','C3',88.00),('PS002','C4',80.00),('PS002','C5',90.00),('PS002','C6',70.00),('PS002','C7',75.00),('PS002','C8',80.00),('PS002','C9',90.00),('PS003','C1',80.00),('PS003','C10',87.00),('PS003','C11',40.00),('PS003','C2',87.00),('PS003','C3',86.00),('PS003','C4',90.00),('PS003','C5',95.00),('PS003','C6',85.00),('PS003','C7',89.00),('PS003','C8',85.00),('PS003','C9',60.00),('PS004','C1',87.00),('PS004','C10',75.00),('PS004','C11',60.00),('PS004','C2',80.00),('PS004','C3',98.00),('PS004','C4',89.00),('PS004','C5',89.00),('PS004','C6',83.00),('PS004','C7',85.00),('PS004','C8',91.00),('PS004','C9',30.00),('PS005','C1',90.00),('PS005','C10',70.00),('PS005','C11',50.00),('PS005','C2',85.00),('PS005','C3',88.00),('PS005','C4',70.00),('PS005','C5',85.00),('PS005','C6',85.00),('PS005','C7',90.00),('PS005','C8',86.00),('PS005','C9',50.00),('PS006','C1',70.00),('PS006','C10',82.00),('PS006','C11',30.00),('PS006','C2',90.00),('PS006','C3',87.00),('PS006','C4',75.00),('PS006','C5',70.00),('PS006','C6',70.00),('PS006','C7',60.00),('PS006','C8',95.00),('PS006','C9',55.00),('PS007','C1',89.00),('PS007','C10',76.00),('PS007','C11',80.00),('PS007','C2',83.00),('PS007','C3',89.00),('PS007','C4',91.00),('PS007','C5',90.00),('PS007','C6',88.00),('PS007','C7',75.00),('PS007','C8',50.00),('PS007','C9',88.00),('PS008','C1',86.00),('PS008','C10',65.00),('PS008','C11',40.00),('PS008','C2',75.00),('PS008','C3',88.00),('PS008','C4',87.00),('PS008','C5',89.00),('PS008','C6',80.00),('PS008','C7',80.00),('PS008','C8',60.00),('PS008','C9',87.00),('PS009','C1',70.00),('PS009','C10',86.00),('PS009','C11',40.00),('PS009','C2',90.00),('PS009','C3',80.00),('PS009','C4',95.00),('PS009','C5',95.00),('PS009','C6',70.00),('PS009','C7',75.00),('PS009','C8',86.00),('PS009','C9',65.00),('PS010','C1',89.00),('PS010','C10',65.00),('PS010','C11',56.00),('PS010','C2',75.00),('PS010','C3',92.00),('PS010','C4',94.00),('PS010','C5',95.00),('PS010','C6',80.00),('PS010','C7',87.00),('PS010','C8',40.00),('PS010','C9',70.00),('PS011','C1',75.00),('PS011','C10',100.00),('PS011','C11',20.00),('PS011','C2',87.00),('PS011','C3',88.00),('PS011','C4',70.00),('PS011','C5',65.00),('PS011','C6',50.00),('PS011','C7',60.00),('PS011','C8',89.00),('PS011','C9',10.00),('PS012','C1',70.00),('PS012','C10',88.00),('PS012','C11',60.00),('PS012','C2',90.00),('PS012','C3',75.00),('PS012','C4',86.00),('PS012','C5',87.00),('PS012','C6',60.00),('PS012','C7',70.00),('PS012','C8',90.00),('PS012','C9',40.00),('PS013','C1',60.00),('PS013','C10',78.00),('PS013','C11',50.00),('PS013','C2',50.00),('PS013','C3',80.00),('PS013','C4',77.00),('PS013','C5',86.00),('PS013','C6',88.00),('PS013','C7',67.00),('PS013','C8',70.00),('PS013','C9',77.00),('PS014','C1',50.00),('PS014','C10',86.00),('PS014','C11',30.00),('PS014','C2',60.00),('PS014','C3',65.00),('PS014','C4',70.00),('PS014','C5',75.00),('PS014','C6',65.00),('PS014','C7',70.00),('PS014','C8',75.00),('PS014','C9',80.00),('PS015','C1',70.00),('PS015','C10',75.00),('PS015','C11',30.00),('PS015','C2',75.00),('PS015','C3',75.00),('PS015','C4',70.00),('PS015','C5',80.00),('PS015','C6',60.00),('PS015','C7',75.00),('PS015','C8',70.00),('PS015','C9',65.00),('PS016','C1',75.00),('PS016','C10',75.00),('PS016','C11',80.00),('PS016','C2',85.00),('PS016','C3',77.00),('PS016','C4',86.00),('PS016','C5',80.00),('PS016','C6',76.00),('PS016','C7',77.00),('PS016','C8',76.00),('PS016','C9',60.00);
/*!40000 ALTER TABLE `saw_value` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-18  4:23:40
