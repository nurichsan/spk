<?php
$cat = AhpCategory::model()->findAll();
$jml_cri = array();
$bbt_cri = array();
$min_cri = array();
$max_cri = array();
foreach ($cat as $xc) :
    ?>
    <table class="table table-bordered">
        <tr>
            <?php
            $cri = AhpCriteria::model()->findAllByAttributes(array('category_id' => $xc->category_id));
            echo "<th>#</th>";
            foreach ($cri as $xri) {
                echo "<th>" . $xri->label . "</th>";
                $jml_cri[$xri->criteria_id] = $xri->jml_topsis;
                $bbt_cri[$xri->criteria_id] = $xri->bobot;
                $min_cri[$xri->criteria_id] = 0;
                $max_cri[$xri->criteria_id] = 0;
            }
            ?>
        </tr>
        <?php
        $data = TopsisCalculation::getTabel($xc->category_id);
        $crt_alt = new CDbCriteria();
        $crt_alt->join = "INNER JOIN ahp_mapping a ON a.alternative_id=t.alternative_id";
        $crt_alt->addColumnCondition(array('a.category_id'=>$xc->category_id));
        $altv = AhpAlternative::model()->findAll($crt_alt);
        foreach ($altv as $xal) {
            echo '<tr>';
            echo '<td>' . $xal->alternative_name . '</td>';
            $i = 1;
            foreach ($cri as $zri) {
                //nilai alternative / jml_total nilai alternative
                $val1 = $data[$xal->alternative_id][$zri->criteria_id] / $jml_cri[$zri->criteria_id];
                $val2 = $val1 * $bbt_cri[$zri->criteria_id];
                $val3 = number_format($val2, 3, ',', '.');
                if($min_cri[$zri->criteria_id] == 0 || $min_cri[$zri->criteria_id] == null){
                    $min_cri[$zri->criteria_id] = $val2;
                }
                //if ($zri->type == 'benefit'){
                if ($val2 < $min_cri[$zri->criteria_id]) {
                    $min_cri[$zri->criteria_id] = $val2;
                }
                if ($val2 > $max_cri[$zri->criteria_id]) {
                    $max_cri[$zri->criteria_id] = $val2;
                }
                //}
                echo '<td>' . $val3 . '</td>';
            }
            echo '</tr>';
        }
        
        echo '<tr bgcolor="yellow">';
        echo '<td><b>MIN</b></td>';
        foreach ($cri as $xri) {
            echo '<td>' . number_format($min_cri[$xri->criteria_id], 3, ',', '.') . '</td>';
        }
        echo '</tr>';
        echo '<tr bgcolor="green">';
        echo '<td><b>MAX</b></td>';
        foreach ($cri as $xri) {
            echo '<td>' . number_format($max_cri[$xri->criteria_id], 3, ',', '.') . '</td>';
        }
        echo '</tr>';
//        echo '<tr>';
//        echo '<td>Jumlah</td>';
//        foreach ($cri as $xri) {
//            echo '<td>' . $jml_cri[$xri->criteria_id] . '</td>';
//        }
//        echo '</tr>';

        ?>
    </table>
<?php endforeach; ?>

