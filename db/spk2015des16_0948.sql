/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50611
Source Host           : localhost:3306
Source Database       : spk

Target Server Type    : MYSQL
Target Server Version : 50611
File Encoding         : 65001

Date: 2015-12-16 09:48:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ahp_alternative`
-- ----------------------------
DROP TABLE IF EXISTS `ahp_alternative`;
CREATE TABLE `ahp_alternative` (
  `alternative_id` int(11) NOT NULL AUTO_INCREMENT,
  `alternative_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`alternative_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_alternative
-- ----------------------------
INSERT INTO `ahp_alternative` VALUES ('1', 'Kost 1');
INSERT INTO `ahp_alternative` VALUES ('2', 'Kost 2');
INSERT INTO `ahp_alternative` VALUES ('3', 'Kost 3');
INSERT INTO `ahp_alternative` VALUES ('4', 'Kost 4');

-- ----------------------------
-- Table structure for `ahp_consistency_index`
-- ----------------------------
DROP TABLE IF EXISTS `ahp_consistency_index`;
CREATE TABLE `ahp_consistency_index` (
  `item` int(11) NOT NULL DEFAULT '0',
  `value` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_consistency_index
-- ----------------------------
INSERT INTO `ahp_consistency_index` VALUES ('1', '0.00');
INSERT INTO `ahp_consistency_index` VALUES ('2', '0.00');
INSERT INTO `ahp_consistency_index` VALUES ('3', '0.58');
INSERT INTO `ahp_consistency_index` VALUES ('4', '0.90');
INSERT INTO `ahp_consistency_index` VALUES ('5', '1.12');
INSERT INTO `ahp_consistency_index` VALUES ('6', '1.24');
INSERT INTO `ahp_consistency_index` VALUES ('7', '1.32');
INSERT INTO `ahp_consistency_index` VALUES ('8', '1.41');
INSERT INTO `ahp_consistency_index` VALUES ('9', '1.45');
INSERT INTO `ahp_consistency_index` VALUES ('10', '1.49');

-- ----------------------------
-- Table structure for `ahp_criteria`
-- ----------------------------
DROP TABLE IF EXISTS `ahp_criteria`;
CREATE TABLE `ahp_criteria` (
  `criteria_id` int(11) NOT NULL AUTO_INCREMENT,
  `criteria_name` varchar(50) DEFAULT NULL,
  `type_field` enum('dropdown','textfield') DEFAULT 'textfield',
  `type` enum('benefit','cost') DEFAULT 'benefit',
  PRIMARY KEY (`criteria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_criteria
-- ----------------------------
INSERT INTO `ahp_criteria` VALUES ('1', 'Jarak', 'textfield', 'cost');
INSERT INTO `ahp_criteria` VALUES ('2', 'Harga Per Bulan', 'textfield', 'cost');
INSERT INTO `ahp_criteria` VALUES ('3', 'Ukuran Kamar', 'dropdown', 'benefit');
INSERT INTO `ahp_criteria` VALUES ('4', 'Keadaan Bangunan', 'dropdown', 'benefit');
INSERT INTO `ahp_criteria` VALUES ('5', 'Banyak Kamar', 'textfield', 'benefit');
INSERT INTO `ahp_criteria` VALUES ('6', 'Ukuran Parkir', 'dropdown', 'benefit');

-- ----------------------------
-- Table structure for `ahp_pair_wise`
-- ----------------------------
DROP TABLE IF EXISTS `ahp_pair_wise`;
CREATE TABLE `ahp_pair_wise` (
  `criteria_1` int(11) NOT NULL DEFAULT '0',
  `criteria_2` int(11) NOT NULL DEFAULT '0',
  `value` decimal(14,4) DEFAULT NULL,
  PRIMARY KEY (`criteria_1`,`criteria_2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_pair_wise
-- ----------------------------
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '1.0000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '1.4999');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '2.0000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '3.0003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '3.5002');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '3.0003');
INSERT INTO `ahp_pair_wise` VALUES ('2', '1', '0.6667');
INSERT INTO `ahp_pair_wise` VALUES ('2', '2', '1.0000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '3', '2.5000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '4', '3.0003');
INSERT INTO `ahp_pair_wise` VALUES ('2', '5', '3.5002');
INSERT INTO `ahp_pair_wise` VALUES ('2', '6', '3.0003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '1', '0.5000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '2', '0.4000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '3', '1.0000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '4', '1.4999');
INSERT INTO `ahp_pair_wise` VALUES ('3', '5', '2.0000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '6', '1.4999');
INSERT INTO `ahp_pair_wise` VALUES ('4', '1', '0.3333');
INSERT INTO `ahp_pair_wise` VALUES ('4', '2', '0.3333');
INSERT INTO `ahp_pair_wise` VALUES ('4', '3', '0.6667');
INSERT INTO `ahp_pair_wise` VALUES ('4', '4', '1.0000');
INSERT INTO `ahp_pair_wise` VALUES ('4', '5', '2.0000');
INSERT INTO `ahp_pair_wise` VALUES ('4', '6', '1.0000');
INSERT INTO `ahp_pair_wise` VALUES ('5', '1', '0.2857');
INSERT INTO `ahp_pair_wise` VALUES ('5', '2', '0.2857');
INSERT INTO `ahp_pair_wise` VALUES ('5', '3', '0.5000');
INSERT INTO `ahp_pair_wise` VALUES ('5', '4', '0.5000');
INSERT INTO `ahp_pair_wise` VALUES ('5', '5', '1.0000');
INSERT INTO `ahp_pair_wise` VALUES ('5', '6', '0.5000');
INSERT INTO `ahp_pair_wise` VALUES ('6', '1', '0.3333');
INSERT INTO `ahp_pair_wise` VALUES ('6', '2', '0.3333');
INSERT INTO `ahp_pair_wise` VALUES ('6', '3', '0.6667');
INSERT INTO `ahp_pair_wise` VALUES ('6', '4', '1.0000');
INSERT INTO `ahp_pair_wise` VALUES ('6', '5', '2.0000');
INSERT INTO `ahp_pair_wise` VALUES ('6', '6', '1.0000');

-- ----------------------------
-- Table structure for `ahp_value`
-- ----------------------------
DROP TABLE IF EXISTS `ahp_value`;
CREATE TABLE `ahp_value` (
  `alternative_id` int(11) NOT NULL DEFAULT '0',
  `criteria_id` int(11) NOT NULL DEFAULT '0',
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`alternative_id`,`criteria_id`),
  KEY `criteria_id` (`criteria_id`),
  CONSTRAINT `ahp_value_ibfk_1` FOREIGN KEY (`alternative_id`) REFERENCES `ahp_alternative` (`alternative_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ahp_value_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `ahp_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_value
-- ----------------------------
INSERT INTO `ahp_value` VALUES ('1', '1', '10');
INSERT INTO `ahp_value` VALUES ('1', '2', '300000');
INSERT INTO `ahp_value` VALUES ('1', '3', '6');
INSERT INTO `ahp_value` VALUES ('1', '4', '4');
INSERT INTO `ahp_value` VALUES ('1', '5', '8');
INSERT INTO `ahp_value` VALUES ('1', '6', '6');
INSERT INTO `ahp_value` VALUES ('2', '1', '5');
INSERT INTO `ahp_value` VALUES ('2', '2', '450000');
INSERT INTO `ahp_value` VALUES ('2', '3', '4');
INSERT INTO `ahp_value` VALUES ('2', '4', '4');
INSERT INTO `ahp_value` VALUES ('2', '5', '5');
INSERT INTO `ahp_value` VALUES ('2', '6', '2');
INSERT INTO `ahp_value` VALUES ('3', '1', '7');
INSERT INTO `ahp_value` VALUES ('3', '2', '400000');
INSERT INTO `ahp_value` VALUES ('3', '3', '2');
INSERT INTO `ahp_value` VALUES ('3', '4', '2');
INSERT INTO `ahp_value` VALUES ('3', '5', '12');
INSERT INTO `ahp_value` VALUES ('3', '6', '4');
INSERT INTO `ahp_value` VALUES ('4', '1', '12');
INSERT INTO `ahp_value` VALUES ('4', '2', '275000');
INSERT INTO `ahp_value` VALUES ('4', '3', '4');
INSERT INTO `ahp_value` VALUES ('4', '4', '4');
INSERT INTO `ahp_value` VALUES ('4', '5', '10');
INSERT INTO `ahp_value` VALUES ('4', '6', '6');

-- ----------------------------
-- Table structure for `person`
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `person_id` varchar(5) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES ('PS001', 'RONALDO');
INSERT INTO `person` VALUES ('PS002', 'RAMOS');
INSERT INTO `person` VALUES ('PS003', 'JAMES');
INSERT INTO `person` VALUES ('PS004', 'BENZEMA');
INSERT INTO `person` VALUES ('PS005', 'BALE');
INSERT INTO `person` VALUES ('PS006', 'VARANE');
INSERT INTO `person` VALUES ('PS007', 'MARCELLO');
INSERT INTO `person` VALUES ('PS008', 'CARVAJAL');
INSERT INTO `person` VALUES ('PS009', 'KROOS');
INSERT INTO `person` VALUES ('PS010', 'MODRIC');
INSERT INTO `person` VALUES ('PS011', 'NAVAS');
INSERT INTO `person` VALUES ('PS012', 'CASEMIRO');
INSERT INTO `person` VALUES ('PS013', 'DANILO');
INSERT INTO `person` VALUES ('PS014', 'ARBELOA');
INSERT INTO `person` VALUES ('PS015', 'NACHO');
INSERT INTO `person` VALUES ('PS016', 'KOVACIC');

-- ----------------------------
-- Table structure for `saw_criteria`
-- ----------------------------
DROP TABLE IF EXISTS `saw_criteria`;
CREATE TABLE `saw_criteria` (
  `criteria_id` varchar(5) NOT NULL DEFAULT '',
  `criteria_name` varchar(100) DEFAULT NULL,
  `prosen` decimal(14,2) DEFAULT NULL,
  `type` enum('benefit','cost','','') NOT NULL DEFAULT 'benefit',
  `no_urut` int(11) DEFAULT NULL,
  PRIMARY KEY (`criteria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of saw_criteria
-- ----------------------------
INSERT INTO `saw_criteria` VALUES ('C1', 'KELINCAHAN', '9.00', 'benefit', '1');
INSERT INTO `saw_criteria` VALUES ('C10', 'KOGNITIF', '9.00', 'benefit', '10');
INSERT INTO `saw_criteria` VALUES ('C11', 'EMOSIONAL', '5.00', 'cost', '11');
INSERT INTO `saw_criteria` VALUES ('C2', 'KESEIMBANGAN', '10.00', 'benefit', '2');
INSERT INTO `saw_criteria` VALUES ('C3', 'KECEPATAN REAKSI', '11.00', 'benefit', '3');
INSERT INTO `saw_criteria` VALUES ('C4', 'PASSING BAWAH', '12.00', 'benefit', '4');
INSERT INTO `saw_criteria` VALUES ('C5', 'PASSING ATAS', '11.00', 'benefit', '5');
INSERT INTO `saw_criteria` VALUES ('C6', 'DRIBBLING', '8.00', 'benefit', '6');
INSERT INTO `saw_criteria` VALUES ('C7', 'SHOOTING', '10.00', 'benefit', '7');
INSERT INTO `saw_criteria` VALUES ('C8', 'HEADING', '8.00', 'benefit', '8');
INSERT INTO `saw_criteria` VALUES ('C9', 'TACKLING', '7.00', 'cost', '9');

-- ----------------------------
-- Table structure for `saw_value`
-- ----------------------------
DROP TABLE IF EXISTS `saw_value`;
CREATE TABLE `saw_value` (
  `person_id` varchar(5) NOT NULL,
  `criteria_id` varchar(5) NOT NULL,
  `value` decimal(14,2) NOT NULL,
  PRIMARY KEY (`person_id`,`criteria_id`),
  KEY `criteria_id` (`criteria_id`),
  CONSTRAINT `saw_value_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `saw_value_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `saw_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of saw_value
-- ----------------------------
INSERT INTO `saw_value` VALUES ('PS001', 'C1', '90.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C10', '95.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C11', '10.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C2', '85.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C3', '90.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C5', '80.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C6', '90.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C7', '96.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C8', '90.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C9', '60.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C1', '75.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C10', '80.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C11', '70.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C2', '89.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C3', '88.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C4', '80.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C5', '90.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C6', '70.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C7', '75.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C8', '80.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C9', '90.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C1', '80.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C10', '87.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C11', '40.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C2', '87.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C3', '86.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C4', '90.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C5', '95.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C6', '85.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C7', '89.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C8', '85.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C9', '60.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C1', '87.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C10', '75.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C11', '60.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C2', '80.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C3', '98.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C4', '89.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C5', '89.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C6', '83.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C7', '85.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C8', '91.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C9', '30.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C1', '90.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C10', '70.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C11', '50.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C2', '85.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C3', '88.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C5', '85.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C6', '85.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C7', '90.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C8', '86.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C9', '50.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C1', '70.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C10', '82.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C11', '30.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C2', '90.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C3', '87.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C4', '75.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C5', '70.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C6', '70.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C7', '60.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C8', '95.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C9', '55.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C1', '89.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C10', '76.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C11', '80.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C2', '83.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C3', '89.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C4', '91.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C5', '90.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C6', '88.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C7', '75.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C8', '50.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C9', '88.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C1', '86.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C10', '65.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C11', '40.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C2', '75.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C3', '88.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C4', '87.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C5', '89.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C6', '80.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C7', '80.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C8', '60.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C9', '87.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C1', '70.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C10', '86.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C11', '40.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C2', '90.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C3', '80.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C4', '95.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C5', '95.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C6', '70.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C7', '75.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C8', '86.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C9', '65.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C1', '89.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C10', '65.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C11', '56.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C2', '75.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C3', '92.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C4', '94.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C5', '95.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C6', '80.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C7', '87.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C8', '40.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C9', '70.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C1', '75.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C10', '100.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C11', '20.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C2', '87.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C3', '88.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C5', '65.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C6', '50.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C7', '60.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C8', '89.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C9', '10.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C1', '70.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C10', '88.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C11', '60.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C2', '90.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C3', '75.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C4', '86.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C5', '87.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C6', '60.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C7', '70.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C8', '90.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C9', '40.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C1', '60.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C10', '78.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C11', '50.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C2', '50.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C3', '80.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C4', '77.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C5', '86.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C6', '88.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C7', '67.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C8', '70.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C9', '77.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C1', '50.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C10', '86.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C11', '30.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C2', '60.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C3', '65.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C5', '75.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C6', '65.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C7', '70.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C8', '75.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C9', '80.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C1', '70.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C10', '75.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C11', '30.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C2', '75.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C3', '75.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C5', '80.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C6', '60.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C7', '75.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C8', '70.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C9', '65.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C1', '75.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C10', '75.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C11', '80.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C2', '85.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C3', '77.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C4', '86.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C5', '80.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C6', '76.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C7', '77.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C8', '76.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C9', '60.00');
