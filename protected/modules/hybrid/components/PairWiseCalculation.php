<?php

/**
 * Description of PairWise
 *
 * @author Nur Ichsan
 */
class PairWiseCalculation {

    public static function getData($all_criteria, $category_id) {
        /*
         * kolom akar total kolom - 2;
         * kolom bobot total_kolom -1;
         */
        $result = array();
        $data = self::getTable($category_id);
        $i = 0;
        foreach ($all_criteria as $val1) {
            $produk = 1;
            $j = 0;
            $result[$i][$j] = $val1->label;
            $j++;
            foreach ($all_criteria as $val2) {
                $result[$i][$j] = $data[$val1->criteria_id][$val2->criteria_id];
                $produk *= $result[$i][$j]; //menghitung produk
                $j++;
            }
            $result[$i][$j] = number_format($produk, 3, '.', ',');
            $j++;
            $akar = pow($produk, (1 / count($all_criteria))); //menghitung akar
            $result[$i][$j] = number_format($akar, 3, '.', ',');
            $j++;
            $result[$i][$j] = number_format($akar / self::totalAkar($all_criteria, $data), 3, '.', ','); //menghitung bobot
            $bobot = $result[$i][$j];
            $sql = "UPDATE ahp_criteria SET bobot = :param WHERE criteria_id = :param2";
            $qry = Yii::app()->db->createCommand($sql);
            $qry->bindParam(':param', $bobot);
            $qry->bindParam(':param2', $val1->criteria_id);
            $qry->execute();
            $i++;
        }

        return $result;
    }

    public static function totalAkar($all_criteria, $data) {
        $result = 0;
        foreach ($all_criteria as $val1) {
            $produk = 1;
            foreach ($all_criteria as $val2) {
                $value = $data[$val1->criteria_id][$val2->criteria_id];
                $produk *= $value;
            }
            $akar = pow($produk, (1 / count($all_criteria)));
            $result += $akar;
        }
        return $result;
    }

    public static function getTable($id) {
        $m_pairwise = AhpPairWise::model()->findAllByAttributes(array('category_id' => $id));
        $pair_wise = array();
        foreach ($m_pairwise as $row) {
            $pair_wise[$row->criteria_1][$row->criteria_2] = $row->value;
        }
        return $pair_wise;
    }

}

?>
