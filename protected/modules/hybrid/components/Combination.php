<?php

/**
 * Description of Combination
 *
 * @author Nur Ichsan
 */
class Combination {

    public static function factorial($n) {
        if ($n <= 1) {
            return 1;
        } else {
            return self::factorial($n - 1) * $n;
        }
    }

    public static function permutasi($n, $k) {
        return pow($n, $k);
    }

    public static function combinations($n, $k) {
        if ($n < $k) {
            return 0;
        } else {
            return self::factorial($n) / (self::factorial($n - $k));
        }
    }

}

?>
