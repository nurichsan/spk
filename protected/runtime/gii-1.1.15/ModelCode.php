<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => '',
  'modelPath' => 'application.modules.hybrid.models.',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '0',
);
