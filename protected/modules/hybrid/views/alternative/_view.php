<?php
/* @var $this AlternativeController */
/* @var $data AhpAlternative */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('alternative_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->alternative_id), array('view', 'id'=>$data->alternative_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alternative_name')); ?>:</b>
	<?php echo CHtml::encode($data->alternative_name); ?>
	<br />


</div>