<?php
/* @var $this CriteriaController */
/* @var $model SawCriteria */

$this->breadcrumbs=array(
	'Saw Criterias'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SawCriteria', 'url'=>array('index')),
	array('label'=>'Manage SawCriteria', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>