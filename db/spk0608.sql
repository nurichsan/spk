/*
Navicat MySQL Data Transfer

Source Server         : LOCAL
Source Server Version : 50611
Source Host           : 127.0.0.1:3306
Source Database       : spk0518

Target Server Type    : MYSQL
Target Server Version : 50611
File Encoding         : 65001

Date: 2017-06-08 21:23:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ahp_alternative
-- ----------------------------
DROP TABLE IF EXISTS `ahp_alternative`;
CREATE TABLE `ahp_alternative` (
  `alternative_id` int(11) NOT NULL AUTO_INCREMENT,
  `alternative_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`alternative_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_alternative
-- ----------------------------
INSERT INTO `ahp_alternative` VALUES ('1', 'Petugas 1');
INSERT INTO `ahp_alternative` VALUES ('2', 'Petugas 2');
INSERT INTO `ahp_alternative` VALUES ('3', 'Petugas 3');
INSERT INTO `ahp_alternative` VALUES ('4', 'Petugas 4');
INSERT INTO `ahp_alternative` VALUES ('5', 'Petugas 5');
INSERT INTO `ahp_alternative` VALUES ('6', 'Petugas 6');
INSERT INTO `ahp_alternative` VALUES ('7', 'Petugas 7');
INSERT INTO `ahp_alternative` VALUES ('8', 'Petugas 8');
INSERT INTO `ahp_alternative` VALUES ('9', 'Petugas 9');
INSERT INTO `ahp_alternative` VALUES ('10', 'Petugas 10');

-- ----------------------------
-- Table structure for ahp_category
-- ----------------------------
DROP TABLE IF EXISTS `ahp_category`;
CREATE TABLE `ahp_category` (
  `category_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_category
-- ----------------------------
INSERT INTO `ahp_category` VALUES ('1', 'Umum');
INSERT INTO `ahp_category` VALUES ('2', 'Ruangan');
INSERT INTO `ahp_category` VALUES ('3', 'Halaman');

-- ----------------------------
-- Table structure for ahp_consistency_index
-- ----------------------------
DROP TABLE IF EXISTS `ahp_consistency_index`;
CREATE TABLE `ahp_consistency_index` (
  `item` int(11) NOT NULL DEFAULT '0',
  `value` decimal(14,2) DEFAULT NULL,
  PRIMARY KEY (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_consistency_index
-- ----------------------------
INSERT INTO `ahp_consistency_index` VALUES ('1', '0.00');
INSERT INTO `ahp_consistency_index` VALUES ('2', '0.00');
INSERT INTO `ahp_consistency_index` VALUES ('3', '0.58');
INSERT INTO `ahp_consistency_index` VALUES ('4', '0.90');
INSERT INTO `ahp_consistency_index` VALUES ('5', '1.12');
INSERT INTO `ahp_consistency_index` VALUES ('6', '1.24');
INSERT INTO `ahp_consistency_index` VALUES ('7', '1.32');
INSERT INTO `ahp_consistency_index` VALUES ('8', '1.41');
INSERT INTO `ahp_consistency_index` VALUES ('9', '1.45');
INSERT INTO `ahp_consistency_index` VALUES ('10', '1.49');
INSERT INTO `ahp_consistency_index` VALUES ('11', '1.51');
INSERT INTO `ahp_consistency_index` VALUES ('12', '1.48');
INSERT INTO `ahp_consistency_index` VALUES ('13', '1.56');
INSERT INTO `ahp_consistency_index` VALUES ('14', '1.57');
INSERT INTO `ahp_consistency_index` VALUES ('15', '1.59');

-- ----------------------------
-- Table structure for ahp_criteria
-- ----------------------------
DROP TABLE IF EXISTS `ahp_criteria`;
CREATE TABLE `ahp_criteria` (
  `criteria_id` int(11) NOT NULL AUTO_INCREMENT,
  `criteria_name` varchar(50) DEFAULT NULL,
  `label` varchar(3) DEFAULT NULL,
  `type_field` enum('dropdown','textfield') DEFAULT 'textfield',
  `type` enum('benefit','cost') DEFAULT 'benefit',
  `category_id` bigint(20) DEFAULT NULL,
  `bobot` decimal(14,3) DEFAULT NULL,
  `jml_topsis` decimal(14,2) DEFAULT '0.00',
  PRIMARY KEY (`criteria_id`),
  KEY `PCategory` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_criteria
-- ----------------------------
INSERT INTO `ahp_criteria` VALUES ('1', 'Kesetiaan', 'U1', 'textfield', 'benefit', '1', '0.163', '848.00');
INSERT INTO `ahp_criteria` VALUES ('2', 'Prestasi Kerja', 'U2', 'textfield', 'benefit', '1', '0.134', '742.00');
INSERT INTO `ahp_criteria` VALUES ('3', 'Tanggung Jawab', 'U3', 'textfield', 'benefit', '1', '0.125', '851.00');
INSERT INTO `ahp_criteria` VALUES ('4', 'Ketaatan', 'U4', 'textfield', 'benefit', '1', '0.124', '832.00');
INSERT INTO `ahp_criteria` VALUES ('5', 'Kejujuran', 'U5', 'textfield', 'benefit', '1', '0.087', '814.00');
INSERT INTO `ahp_criteria` VALUES ('6', 'Kerjasama', 'U6', 'textfield', 'benefit', '1', '0.083', '873.00');
INSERT INTO `ahp_criteria` VALUES ('7', 'Loyalitas', 'U7', 'textfield', 'benefit', '1', '0.076', '829.00');
INSERT INTO `ahp_criteria` VALUES ('8', 'Kedisplinan', 'U8', 'textfield', 'benefit', '1', '0.066', '844.00');
INSERT INTO `ahp_criteria` VALUES ('9', 'Hubungan Antar Manusia', 'U9', 'textfield', 'benefit', '1', '0.056', '852.00');
INSERT INTO `ahp_criteria` VALUES ('10', 'Kreatifitas', 'U10', 'textfield', 'benefit', '1', '0.048', '843.00');
INSERT INTO `ahp_criteria` VALUES ('11', 'Prestasi', 'U11', 'textfield', 'benefit', '1', '0.038', '861.00');
INSERT INTO `ahp_criteria` VALUES ('12', 'Mengeluarkan Sampah', 'R1', 'textfield', 'benefit', '2', '0.202', '502.00');
INSERT INTO `ahp_criteria` VALUES ('13', 'Membersihkan Tempat Sampah', 'R2', 'textfield', 'benefit', '2', '0.143', '471.00');
INSERT INTO `ahp_criteria` VALUES ('14', 'Membersihkan Furniture', 'R3', 'textfield', 'benefit', '2', '0.150', '492.00');
INSERT INTO `ahp_criteria` VALUES ('15', 'Menyapu dan Mengepel Lantai', 'R4', 'textfield', 'benefit', '2', '0.105', '511.00');
INSERT INTO `ahp_criteria` VALUES ('16', 'Membersihkan KM, WC dan Watafel', 'R5', 'textfield', 'benefit', '2', '0.126', '479.00');
INSERT INTO `ahp_criteria` VALUES ('17', 'Membersihkan Pintu, Kaca dan Jendela', 'R6', 'textfield', 'benefit', '2', '0.096', '518.00');
INSERT INTO `ahp_criteria` VALUES ('18', 'Membersihkan Dindin, Ventilasi dan Langit-Langit', 'R7', 'textfield', 'benefit', '2', '0.097', '521.00');
INSERT INTO `ahp_criteria` VALUES ('19', 'Pengelolaan Peralatan Kerja', 'R8', 'textfield', 'benefit', '2', '0.081', '522.00');
INSERT INTO `ahp_criteria` VALUES ('20', 'Menyapu', 'H1', 'textfield', 'benefit', '3', '0.288', '322.00');
INSERT INTO `ahp_criteria` VALUES ('21', 'Membersihkan Saluran Hujan', 'H2', 'textfield', 'benefit', '3', '0.205', '325.00');
INSERT INTO `ahp_criteria` VALUES ('22', 'Membuang Sampah ke TPS', 'H3', 'textfield', 'benefit', '3', '0.147', '329.00');
INSERT INTO `ahp_criteria` VALUES ('23', 'Mencuci Tampat Sampah', 'H4', 'textfield', 'benefit', '3', '0.116', '350.00');
INSERT INTO `ahp_criteria` VALUES ('24', 'Menguras Kolam Ikan', 'H5', 'textfield', 'benefit', '3', '0.099', '336.00');
INSERT INTO `ahp_criteria` VALUES ('25', 'Merapihkan Taman', 'H6', 'textfield', 'benefit', '3', '0.053', '332.00');
INSERT INTO `ahp_criteria` VALUES ('26', 'Kerja Bakti', 'H7', 'textfield', 'benefit', '3', '0.048', '306.00');
INSERT INTO `ahp_criteria` VALUES ('27', 'Pengelolaan Peralatan Kerja', 'H8', 'textfield', 'benefit', '3', '0.043', '342.00');

-- ----------------------------
-- Table structure for ahp_mapping
-- ----------------------------
DROP TABLE IF EXISTS `ahp_mapping`;
CREATE TABLE `ahp_mapping` (
  `mapping_id` int(11) NOT NULL AUTO_INCREMENT,
  `alternative_id` int(11) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`mapping_id`),
  KEY `alternative_id` (`alternative_id`),
  CONSTRAINT `ahp_mapping_ibfk_1` FOREIGN KEY (`alternative_id`) REFERENCES `ahp_alternative` (`alternative_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_mapping
-- ----------------------------
INSERT INTO `ahp_mapping` VALUES ('1', '1', '1');
INSERT INTO `ahp_mapping` VALUES ('2', '2', '1');
INSERT INTO `ahp_mapping` VALUES ('3', '3', '1');
INSERT INTO `ahp_mapping` VALUES ('4', '4', '1');
INSERT INTO `ahp_mapping` VALUES ('5', '5', '1');
INSERT INTO `ahp_mapping` VALUES ('6', '6', '1');
INSERT INTO `ahp_mapping` VALUES ('7', '7', '1');
INSERT INTO `ahp_mapping` VALUES ('8', '8', '1');
INSERT INTO `ahp_mapping` VALUES ('9', '9', '1');
INSERT INTO `ahp_mapping` VALUES ('10', '10', '1');
INSERT INTO `ahp_mapping` VALUES ('16', '1', '2');
INSERT INTO `ahp_mapping` VALUES ('17', '2', '2');
INSERT INTO `ahp_mapping` VALUES ('18', '3', '2');
INSERT INTO `ahp_mapping` VALUES ('19', '4', '2');
INSERT INTO `ahp_mapping` VALUES ('20', '5', '2');
INSERT INTO `ahp_mapping` VALUES ('21', '6', '2');
INSERT INTO `ahp_mapping` VALUES ('22', '7', '3');
INSERT INTO `ahp_mapping` VALUES ('23', '8', '3');
INSERT INTO `ahp_mapping` VALUES ('24', '9', '3');
INSERT INTO `ahp_mapping` VALUES ('25', '10', '3');

-- ----------------------------
-- Table structure for ahp_pair_wise
-- ----------------------------
DROP TABLE IF EXISTS `ahp_pair_wise`;
CREATE TABLE `ahp_pair_wise` (
  `category_id` int(11) NOT NULL,
  `criteria_1` int(11) NOT NULL DEFAULT '0',
  `criteria_2` int(11) NOT NULL DEFAULT '0',
  `value` decimal(14,3) DEFAULT NULL,
  PRIMARY KEY (`criteria_1`,`criteria_2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_pair_wise
-- ----------------------------
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '1', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '2', '3.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '3', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '4', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '5', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '6', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '7', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '8', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '9', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '10', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '1', '11', '3.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '1', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '2', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '3', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '4', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '5', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '6', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '7', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '8', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '9', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '10', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '2', '11', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '1', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '2', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '3', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '4', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '5', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '6', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '7', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '8', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '9', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '10', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '3', '11', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '1', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '2', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '3', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '4', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '5', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '6', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '7', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '8', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '9', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '10', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '4', '11', '3.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '1', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '2', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '3', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '4', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '5', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '6', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '7', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '8', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '9', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '10', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '5', '11', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '1', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '2', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '3', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '4', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '5', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '6', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '7', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '8', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '9', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '10', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '6', '11', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '1', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '2', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '3', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '4', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '5', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '6', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '7', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '8', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '9', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '10', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '7', '11', '3.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '1', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '2', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '3', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '4', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '5', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '6', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '7', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '8', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '9', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '10', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '8', '11', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '1', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '2', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '3', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '4', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '5', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '6', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '7', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '8', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '9', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '10', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '9', '11', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '1', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '2', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '3', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '4', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '5', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '6', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '7', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '8', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '9', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '10', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '10', '11', '3.000');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '1', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '2', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '3', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '4', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '5', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '6', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '7', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '8', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '9', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '10', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('1', '11', '11', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '12', '12', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '12', '13', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '12', '14', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '12', '15', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '12', '16', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '12', '17', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '12', '18', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '12', '19', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '13', '12', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '13', '13', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '13', '14', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '13', '15', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '13', '16', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '13', '17', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '13', '18', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '13', '19', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '14', '12', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '14', '13', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '14', '14', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '14', '15', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('2', '14', '16', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '14', '17', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '14', '18', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '14', '19', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '15', '12', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '15', '13', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '15', '14', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('2', '15', '15', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '15', '16', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '15', '17', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '15', '18', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '15', '19', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '16', '12', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '16', '13', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '16', '14', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '16', '15', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '16', '16', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '16', '17', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('2', '16', '18', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '16', '19', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '17', '12', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '17', '13', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '17', '14', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '17', '15', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '17', '16', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('2', '17', '17', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '17', '18', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '17', '19', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '18', '12', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '18', '13', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '18', '14', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '18', '15', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '18', '16', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '18', '17', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '18', '18', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '18', '19', '3.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '19', '12', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '19', '13', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '19', '14', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '19', '15', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '19', '16', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('2', '19', '17', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('2', '19', '18', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('2', '19', '19', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '20', '20', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '20', '21', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '20', '22', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '20', '23', '4.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '20', '24', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '20', '25', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '20', '26', '5.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '20', '27', '5.988');
INSERT INTO `ahp_pair_wise` VALUES ('3', '21', '20', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '21', '21', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '21', '22', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '21', '23', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '21', '24', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '21', '25', '4.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '21', '26', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '21', '27', '4.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '22', '20', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '22', '21', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '22', '22', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '22', '23', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '22', '24', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '22', '25', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '22', '26', '5.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '22', '27', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '23', '20', '0.250');
INSERT INTO `ahp_pair_wise` VALUES ('3', '23', '21', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '23', '22', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '23', '23', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '23', '24', '4.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '23', '25', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '23', '26', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '23', '27', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '24', '20', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '24', '21', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '24', '22', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '24', '23', '0.250');
INSERT INTO `ahp_pair_wise` VALUES ('3', '24', '24', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '24', '25', '5.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '24', '26', '4.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '24', '27', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '25', '20', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '25', '21', '0.250');
INSERT INTO `ahp_pair_wise` VALUES ('3', '25', '22', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '25', '23', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '25', '24', '0.200');
INSERT INTO `ahp_pair_wise` VALUES ('3', '25', '25', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '25', '26', '2.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '25', '27', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '26', '20', '0.200');
INSERT INTO `ahp_pair_wise` VALUES ('3', '26', '21', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '26', '22', '0.200');
INSERT INTO `ahp_pair_wise` VALUES ('3', '26', '23', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '26', '24', '0.250');
INSERT INTO `ahp_pair_wise` VALUES ('3', '26', '25', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '26', '26', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '26', '27', '3.003');
INSERT INTO `ahp_pair_wise` VALUES ('3', '27', '20', '0.167');
INSERT INTO `ahp_pair_wise` VALUES ('3', '27', '21', '0.250');
INSERT INTO `ahp_pair_wise` VALUES ('3', '27', '22', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '27', '23', '0.500');
INSERT INTO `ahp_pair_wise` VALUES ('3', '27', '24', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '27', '25', '1.000');
INSERT INTO `ahp_pair_wise` VALUES ('3', '27', '26', '0.333');
INSERT INTO `ahp_pair_wise` VALUES ('3', '27', '27', '1.000');

-- ----------------------------
-- Table structure for ahp_value
-- ----------------------------
DROP TABLE IF EXISTS `ahp_value`;
CREATE TABLE `ahp_value` (
  `alternative_id` int(11) NOT NULL DEFAULT '0',
  `criteria_id` int(11) NOT NULL DEFAULT '0',
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`alternative_id`,`criteria_id`),
  KEY `criteria_id` (`criteria_id`),
  CONSTRAINT `ahp_value_ibfk_1` FOREIGN KEY (`alternative_id`) REFERENCES `ahp_alternative` (`alternative_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ahp_value_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `ahp_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ahp_value
-- ----------------------------
INSERT INTO `ahp_value` VALUES ('1', '1', '87');
INSERT INTO `ahp_value` VALUES ('1', '2', '71');
INSERT INTO `ahp_value` VALUES ('1', '3', '87');
INSERT INTO `ahp_value` VALUES ('1', '4', '91');
INSERT INTO `ahp_value` VALUES ('1', '5', '88');
INSERT INTO `ahp_value` VALUES ('1', '6', '90');
INSERT INTO `ahp_value` VALUES ('1', '7', '90');
INSERT INTO `ahp_value` VALUES ('1', '8', '90');
INSERT INTO `ahp_value` VALUES ('1', '9', '81');
INSERT INTO `ahp_value` VALUES ('1', '10', '89');
INSERT INTO `ahp_value` VALUES ('1', '11', '91');
INSERT INTO `ahp_value` VALUES ('1', '12', '80');
INSERT INTO `ahp_value` VALUES ('1', '13', '87');
INSERT INTO `ahp_value` VALUES ('1', '14', '87');
INSERT INTO `ahp_value` VALUES ('1', '15', '87');
INSERT INTO `ahp_value` VALUES ('1', '16', '71');
INSERT INTO `ahp_value` VALUES ('1', '17', '88');
INSERT INTO `ahp_value` VALUES ('1', '18', '90');
INSERT INTO `ahp_value` VALUES ('1', '19', '81');
INSERT INTO `ahp_value` VALUES ('1', '20', '89');
INSERT INTO `ahp_value` VALUES ('1', '21', '80');
INSERT INTO `ahp_value` VALUES ('1', '22', '87');
INSERT INTO `ahp_value` VALUES ('1', '23', '86');
INSERT INTO `ahp_value` VALUES ('1', '24', '71');
INSERT INTO `ahp_value` VALUES ('1', '25', '81');
INSERT INTO `ahp_value` VALUES ('1', '26', '90');
INSERT INTO `ahp_value` VALUES ('1', '27', '80');
INSERT INTO `ahp_value` VALUES ('2', '1', '84');
INSERT INTO `ahp_value` VALUES ('2', '2', '72');
INSERT INTO `ahp_value` VALUES ('2', '3', '90');
INSERT INTO `ahp_value` VALUES ('2', '4', '93');
INSERT INTO `ahp_value` VALUES ('2', '5', '76');
INSERT INTO `ahp_value` VALUES ('2', '6', '93');
INSERT INTO `ahp_value` VALUES ('2', '7', '89');
INSERT INTO `ahp_value` VALUES ('2', '8', '89');
INSERT INTO `ahp_value` VALUES ('2', '9', '93');
INSERT INTO `ahp_value` VALUES ('2', '10', '79');
INSERT INTO `ahp_value` VALUES ('2', '11', '89');
INSERT INTO `ahp_value` VALUES ('2', '12', '83');
INSERT INTO `ahp_value` VALUES ('2', '13', '67');
INSERT INTO `ahp_value` VALUES ('2', '14', '87');
INSERT INTO `ahp_value` VALUES ('2', '15', '79');
INSERT INTO `ahp_value` VALUES ('2', '16', '74');
INSERT INTO `ahp_value` VALUES ('2', '17', '87');
INSERT INTO `ahp_value` VALUES ('2', '18', '89');
INSERT INTO `ahp_value` VALUES ('2', '19', '87');
INSERT INTO `ahp_value` VALUES ('2', '20', '84');
INSERT INTO `ahp_value` VALUES ('2', '21', '72');
INSERT INTO `ahp_value` VALUES ('2', '22', '87');
INSERT INTO `ahp_value` VALUES ('2', '23', '93');
INSERT INTO `ahp_value` VALUES ('2', '24', '74');
INSERT INTO `ahp_value` VALUES ('2', '25', '93');
INSERT INTO `ahp_value` VALUES ('2', '26', '89');
INSERT INTO `ahp_value` VALUES ('2', '27', '81');
INSERT INTO `ahp_value` VALUES ('3', '1', '82');
INSERT INTO `ahp_value` VALUES ('3', '2', '73');
INSERT INTO `ahp_value` VALUES ('3', '3', '75');
INSERT INTO `ahp_value` VALUES ('3', '4', '89');
INSERT INTO `ahp_value` VALUES ('3', '5', '89');
INSERT INTO `ahp_value` VALUES ('3', '6', '87');
INSERT INTO `ahp_value` VALUES ('3', '7', '88');
INSERT INTO `ahp_value` VALUES ('3', '8', '90');
INSERT INTO `ahp_value` VALUES ('3', '9', '90');
INSERT INTO `ahp_value` VALUES ('3', '10', '80');
INSERT INTO `ahp_value` VALUES ('3', '11', '88');
INSERT INTO `ahp_value` VALUES ('3', '12', '89');
INSERT INTO `ahp_value` VALUES ('3', '13', '80');
INSERT INTO `ahp_value` VALUES ('3', '14', '75');
INSERT INTO `ahp_value` VALUES ('3', '15', '80');
INSERT INTO `ahp_value` VALUES ('3', '16', '89');
INSERT INTO `ahp_value` VALUES ('3', '17', '89');
INSERT INTO `ahp_value` VALUES ('3', '18', '88');
INSERT INTO `ahp_value` VALUES ('3', '19', '89');
INSERT INTO `ahp_value` VALUES ('3', '20', '87');
INSERT INTO `ahp_value` VALUES ('3', '21', '74');
INSERT INTO `ahp_value` VALUES ('3', '22', '75');
INSERT INTO `ahp_value` VALUES ('3', '23', '86');
INSERT INTO `ahp_value` VALUES ('3', '24', '89');
INSERT INTO `ahp_value` VALUES ('3', '25', '82');
INSERT INTO `ahp_value` VALUES ('3', '26', '88');
INSERT INTO `ahp_value` VALUES ('3', '27', '89');
INSERT INTO `ahp_value` VALUES ('4', '1', '83');
INSERT INTO `ahp_value` VALUES ('4', '2', '79');
INSERT INTO `ahp_value` VALUES ('4', '3', '79');
INSERT INTO `ahp_value` VALUES ('4', '4', '60');
INSERT INTO `ahp_value` VALUES ('4', '5', '67');
INSERT INTO `ahp_value` VALUES ('4', '6', '89');
INSERT INTO `ahp_value` VALUES ('4', '7', '77');
INSERT INTO `ahp_value` VALUES ('4', '8', '59');
INSERT INTO `ahp_value` VALUES ('4', '9', '69');
INSERT INTO `ahp_value` VALUES ('4', '10', '80');
INSERT INTO `ahp_value` VALUES ('4', '11', '87');
INSERT INTO `ahp_value` VALUES ('4', '12', '88');
INSERT INTO `ahp_value` VALUES ('4', '13', '77');
INSERT INTO `ahp_value` VALUES ('4', '14', '77');
INSERT INTO `ahp_value` VALUES ('4', '15', '91');
INSERT INTO `ahp_value` VALUES ('4', '16', '72');
INSERT INTO `ahp_value` VALUES ('4', '17', '90');
INSERT INTO `ahp_value` VALUES ('4', '18', '77');
INSERT INTO `ahp_value` VALUES ('4', '19', '90');
INSERT INTO `ahp_value` VALUES ('4', '20', '83');
INSERT INTO `ahp_value` VALUES ('4', '21', '79');
INSERT INTO `ahp_value` VALUES ('4', '22', '77');
INSERT INTO `ahp_value` VALUES ('4', '23', '60');
INSERT INTO `ahp_value` VALUES ('4', '24', '72');
INSERT INTO `ahp_value` VALUES ('4', '25', '89');
INSERT INTO `ahp_value` VALUES ('4', '26', '77');
INSERT INTO `ahp_value` VALUES ('4', '27', '90');
INSERT INTO `ahp_value` VALUES ('5', '1', '81');
INSERT INTO `ahp_value` VALUES ('5', '2', '78');
INSERT INTO `ahp_value` VALUES ('5', '3', '90');
INSERT INTO `ahp_value` VALUES ('5', '4', '91');
INSERT INTO `ahp_value` VALUES ('5', '5', '80');
INSERT INTO `ahp_value` VALUES ('5', '6', '88');
INSERT INTO `ahp_value` VALUES ('5', '7', '89');
INSERT INTO `ahp_value` VALUES ('5', '8', '83');
INSERT INTO `ahp_value` VALUES ('5', '9', '72');
INSERT INTO `ahp_value` VALUES ('5', '10', '85');
INSERT INTO `ahp_value` VALUES ('5', '11', '83');
INSERT INTO `ahp_value` VALUES ('5', '12', '82');
INSERT INTO `ahp_value` VALUES ('5', '13', '80');
INSERT INTO `ahp_value` VALUES ('5', '14', '90');
INSERT INTO `ahp_value` VALUES ('5', '15', '88');
INSERT INTO `ahp_value` VALUES ('5', '16', '87');
INSERT INTO `ahp_value` VALUES ('5', '17', '76');
INSERT INTO `ahp_value` VALUES ('5', '18', '89');
INSERT INTO `ahp_value` VALUES ('5', '19', '88');
INSERT INTO `ahp_value` VALUES ('5', '20', '79');
INSERT INTO `ahp_value` VALUES ('5', '21', '79');
INSERT INTO `ahp_value` VALUES ('5', '22', '90');
INSERT INTO `ahp_value` VALUES ('5', '23', '86');
INSERT INTO `ahp_value` VALUES ('5', '24', '80');
INSERT INTO `ahp_value` VALUES ('5', '25', '81');
INSERT INTO `ahp_value` VALUES ('5', '26', '89');
INSERT INTO `ahp_value` VALUES ('5', '27', '75');
INSERT INTO `ahp_value` VALUES ('6', '1', '89');
INSERT INTO `ahp_value` VALUES ('6', '2', '77');
INSERT INTO `ahp_value` VALUES ('6', '3', '82');
INSERT INTO `ahp_value` VALUES ('6', '4', '50');
INSERT INTO `ahp_value` VALUES ('6', '5', '91');
INSERT INTO `ahp_value` VALUES ('6', '6', '88');
INSERT INTO `ahp_value` VALUES ('6', '7', '90');
INSERT INTO `ahp_value` VALUES ('6', '8', '94');
INSERT INTO `ahp_value` VALUES ('6', '9', '97');
INSERT INTO `ahp_value` VALUES ('6', '10', '92');
INSERT INTO `ahp_value` VALUES ('6', '11', '82');
INSERT INTO `ahp_value` VALUES ('6', '12', '80');
INSERT INTO `ahp_value` VALUES ('6', '13', '80');
INSERT INTO `ahp_value` VALUES ('6', '14', '76');
INSERT INTO `ahp_value` VALUES ('6', '15', '86');
INSERT INTO `ahp_value` VALUES ('6', '16', '86');
INSERT INTO `ahp_value` VALUES ('6', '17', '88');
INSERT INTO `ahp_value` VALUES ('6', '18', '88');
INSERT INTO `ahp_value` VALUES ('6', '19', '87');
INSERT INTO `ahp_value` VALUES ('6', '20', '89');
INSERT INTO `ahp_value` VALUES ('6', '21', '80');
INSERT INTO `ahp_value` VALUES ('6', '22', '77');
INSERT INTO `ahp_value` VALUES ('6', '23', '86');
INSERT INTO `ahp_value` VALUES ('6', '24', '79');
INSERT INTO `ahp_value` VALUES ('6', '25', '88');
INSERT INTO `ahp_value` VALUES ('6', '26', '90');
INSERT INTO `ahp_value` VALUES ('6', '27', '79');
INSERT INTO `ahp_value` VALUES ('7', '1', '88');
INSERT INTO `ahp_value` VALUES ('7', '2', '76');
INSERT INTO `ahp_value` VALUES ('7', '3', '84');
INSERT INTO `ahp_value` VALUES ('7', '4', '92');
INSERT INTO `ahp_value` VALUES ('7', '5', '79');
INSERT INTO `ahp_value` VALUES ('7', '6', '79');
INSERT INTO `ahp_value` VALUES ('7', '7', '67');
INSERT INTO `ahp_value` VALUES ('7', '8', '82');
INSERT INTO `ahp_value` VALUES ('7', '9', '83');
INSERT INTO `ahp_value` VALUES ('7', '10', '91');
INSERT INTO `ahp_value` VALUES ('7', '11', '84');
INSERT INTO `ahp_value` VALUES ('7', '12', '89');
INSERT INTO `ahp_value` VALUES ('7', '13', '76');
INSERT INTO `ahp_value` VALUES ('7', '14', '88');
INSERT INTO `ahp_value` VALUES ('7', '15', '92');
INSERT INTO `ahp_value` VALUES ('7', '16', '85');
INSERT INTO `ahp_value` VALUES ('7', '17', '80');
INSERT INTO `ahp_value` VALUES ('7', '18', '87');
INSERT INTO `ahp_value` VALUES ('7', '19', '90');
INSERT INTO `ahp_value` VALUES ('7', '20', '75');
INSERT INTO `ahp_value` VALUES ('7', '21', '76');
INSERT INTO `ahp_value` VALUES ('7', '22', '84');
INSERT INTO `ahp_value` VALUES ('7', '23', '92');
INSERT INTO `ahp_value` VALUES ('7', '24', '97');
INSERT INTO `ahp_value` VALUES ('7', '25', '80');
INSERT INTO `ahp_value` VALUES ('7', '26', '67');
INSERT INTO `ahp_value` VALUES ('7', '27', '87');
INSERT INTO `ahp_value` VALUES ('8', '1', '87');
INSERT INTO `ahp_value` VALUES ('8', '2', '75');
INSERT INTO `ahp_value` VALUES ('8', '3', '90');
INSERT INTO `ahp_value` VALUES ('8', '4', '94');
INSERT INTO `ahp_value` VALUES ('8', '5', '80');
INSERT INTO `ahp_value` VALUES ('8', '6', '85');
INSERT INTO `ahp_value` VALUES ('8', '7', '87');
INSERT INTO `ahp_value` VALUES ('8', '8', '90');
INSERT INTO `ahp_value` VALUES ('8', '9', '84');
INSERT INTO `ahp_value` VALUES ('8', '10', '86');
INSERT INTO `ahp_value` VALUES ('8', '11', '90');
INSERT INTO `ahp_value` VALUES ('8', '12', '87');
INSERT INTO `ahp_value` VALUES ('8', '13', '88');
INSERT INTO `ahp_value` VALUES ('8', '14', '90');
INSERT INTO `ahp_value` VALUES ('8', '15', '86');
INSERT INTO `ahp_value` VALUES ('8', '16', '89');
INSERT INTO `ahp_value` VALUES ('8', '17', '85');
INSERT INTO `ahp_value` VALUES ('8', '18', '80');
INSERT INTO `ahp_value` VALUES ('8', '19', '89');
INSERT INTO `ahp_value` VALUES ('8', '20', '87');
INSERT INTO `ahp_value` VALUES ('8', '21', '88');
INSERT INTO `ahp_value` VALUES ('8', '22', '77');
INSERT INTO `ahp_value` VALUES ('8', '23', '86');
INSERT INTO `ahp_value` VALUES ('8', '24', '80');
INSERT INTO `ahp_value` VALUES ('8', '25', '85');
INSERT INTO `ahp_value` VALUES ('8', '26', '87');
INSERT INTO `ahp_value` VALUES ('8', '27', '89');
INSERT INTO `ahp_value` VALUES ('9', '1', '83');
INSERT INTO `ahp_value` VALUES ('9', '2', '71');
INSERT INTO `ahp_value` VALUES ('9', '3', '91');
INSERT INTO `ahp_value` VALUES ('9', '4', '80');
INSERT INTO `ahp_value` VALUES ('9', '5', '95');
INSERT INTO `ahp_value` VALUES ('9', '6', '87');
INSERT INTO `ahp_value` VALUES ('9', '7', '78');
INSERT INTO `ahp_value` VALUES ('9', '8', '88');
INSERT INTO `ahp_value` VALUES ('9', '9', '90');
INSERT INTO `ahp_value` VALUES ('9', '10', '80');
INSERT INTO `ahp_value` VALUES ('9', '11', '88');
INSERT INTO `ahp_value` VALUES ('9', '12', '85');
INSERT INTO `ahp_value` VALUES ('9', '13', '71');
INSERT INTO `ahp_value` VALUES ('9', '14', '81');
INSERT INTO `ahp_value` VALUES ('9', '15', '80');
INSERT INTO `ahp_value` VALUES ('9', '16', '88');
INSERT INTO `ahp_value` VALUES ('9', '17', '80');
INSERT INTO `ahp_value` VALUES ('9', '18', '83');
INSERT INTO `ahp_value` VALUES ('9', '19', '80');
INSERT INTO `ahp_value` VALUES ('9', '20', '76');
INSERT INTO `ahp_value` VALUES ('9', '21', '71');
INSERT INTO `ahp_value` VALUES ('9', '22', '91');
INSERT INTO `ahp_value` VALUES ('9', '23', '80');
INSERT INTO `ahp_value` VALUES ('9', '24', '90');
INSERT INTO `ahp_value` VALUES ('9', '25', '80');
INSERT INTO `ahp_value` VALUES ('9', '26', '78');
INSERT INTO `ahp_value` VALUES ('9', '27', '80');
INSERT INTO `ahp_value` VALUES ('10', '1', '84');
INSERT INTO `ahp_value` VALUES ('10', '2', '70');
INSERT INTO `ahp_value` VALUES ('10', '3', '83');
INSERT INTO `ahp_value` VALUES ('10', '4', '92');
INSERT INTO `ahp_value` VALUES ('10', '5', '69');
INSERT INTO `ahp_value` VALUES ('10', '6', '87');
INSERT INTO `ahp_value` VALUES ('10', '7', '74');
INSERT INTO `ahp_value` VALUES ('10', '8', '79');
INSERT INTO `ahp_value` VALUES ('10', '9', '93');
INSERT INTO `ahp_value` VALUES ('10', '10', '81');
INSERT INTO `ahp_value` VALUES ('10', '11', '79');
INSERT INTO `ahp_value` VALUES ('10', '12', '89');
INSERT INTO `ahp_value` VALUES ('10', '13', '90');
INSERT INTO `ahp_value` VALUES ('10', '14', '85');
INSERT INTO `ahp_value` VALUES ('10', '15', '92');
INSERT INTO `ahp_value` VALUES ('10', '16', '90');
INSERT INTO `ahp_value` VALUES ('10', '17', '87');
INSERT INTO `ahp_value` VALUES ('10', '18', '85');
INSERT INTO `ahp_value` VALUES ('10', '19', '86');
INSERT INTO `ahp_value` VALUES ('10', '20', '84');
INSERT INTO `ahp_value` VALUES ('10', '21', '90');
INSERT INTO `ahp_value` VALUES ('10', '22', '77');
INSERT INTO `ahp_value` VALUES ('10', '23', '92');
INSERT INTO `ahp_value` VALUES ('10', '24', '69');
INSERT INTO `ahp_value` VALUES ('10', '25', '87');
INSERT INTO `ahp_value` VALUES ('10', '26', '74');
INSERT INTO `ahp_value` VALUES ('10', '27', '86');

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `person_id` varchar(5) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES ('PS001', 'RONALDO');
INSERT INTO `person` VALUES ('PS002', 'RAMOS');
INSERT INTO `person` VALUES ('PS003', 'JAMES');
INSERT INTO `person` VALUES ('PS004', 'BENZEMA');
INSERT INTO `person` VALUES ('PS005', 'BALE');
INSERT INTO `person` VALUES ('PS006', 'VARANE');
INSERT INTO `person` VALUES ('PS007', 'MARCELLO');
INSERT INTO `person` VALUES ('PS008', 'CARVAJAL');
INSERT INTO `person` VALUES ('PS009', 'KROOS');
INSERT INTO `person` VALUES ('PS010', 'MODRIC');
INSERT INTO `person` VALUES ('PS011', 'NAVAS');
INSERT INTO `person` VALUES ('PS012', 'CASEMIRO');
INSERT INTO `person` VALUES ('PS013', 'DANILO');
INSERT INTO `person` VALUES ('PS014', 'ARBELOA');
INSERT INTO `person` VALUES ('PS015', 'NACHO');
INSERT INTO `person` VALUES ('PS016', 'KOVACIC');

-- ----------------------------
-- Table structure for saw_criteria
-- ----------------------------
DROP TABLE IF EXISTS `saw_criteria`;
CREATE TABLE `saw_criteria` (
  `criteria_id` varchar(5) NOT NULL DEFAULT '',
  `criteria_name` varchar(100) DEFAULT NULL,
  `prosen` decimal(14,2) DEFAULT NULL,
  `type` enum('benefit','cost','','') NOT NULL DEFAULT 'benefit',
  `no_urut` int(11) DEFAULT NULL,
  PRIMARY KEY (`criteria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of saw_criteria
-- ----------------------------
INSERT INTO `saw_criteria` VALUES ('C1', 'KELINCAHAN', '9.00', 'benefit', '1');
INSERT INTO `saw_criteria` VALUES ('C10', 'KOGNITIF', '9.00', 'benefit', '10');
INSERT INTO `saw_criteria` VALUES ('C11', 'EMOSIONAL', '5.00', 'cost', '11');
INSERT INTO `saw_criteria` VALUES ('C2', 'KESEIMBANGAN', '10.00', 'benefit', '2');
INSERT INTO `saw_criteria` VALUES ('C3', 'KECEPATAN REAKSI', '11.00', 'benefit', '3');
INSERT INTO `saw_criteria` VALUES ('C4', 'PASSING BAWAH', '12.00', 'benefit', '4');
INSERT INTO `saw_criteria` VALUES ('C5', 'PASSING ATAS', '11.00', 'benefit', '5');
INSERT INTO `saw_criteria` VALUES ('C6', 'DRIBBLING', '8.00', 'benefit', '6');
INSERT INTO `saw_criteria` VALUES ('C7', 'SHOOTING', '10.00', 'benefit', '7');
INSERT INTO `saw_criteria` VALUES ('C8', 'HEADING', '8.00', 'benefit', '8');
INSERT INTO `saw_criteria` VALUES ('C9', 'TACKLING', '7.00', 'cost', '9');

-- ----------------------------
-- Table structure for saw_value
-- ----------------------------
DROP TABLE IF EXISTS `saw_value`;
CREATE TABLE `saw_value` (
  `person_id` varchar(5) NOT NULL,
  `criteria_id` varchar(5) NOT NULL,
  `value` decimal(14,2) NOT NULL,
  PRIMARY KEY (`person_id`,`criteria_id`),
  KEY `criteria_id` (`criteria_id`),
  CONSTRAINT `saw_value_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `saw_value_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `saw_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of saw_value
-- ----------------------------
INSERT INTO `saw_value` VALUES ('PS001', 'C1', '90.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C10', '95.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C11', '10.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C2', '85.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C3', '90.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C5', '80.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C6', '90.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C7', '96.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C8', '90.00');
INSERT INTO `saw_value` VALUES ('PS001', 'C9', '60.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C1', '75.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C10', '80.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C11', '70.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C2', '89.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C3', '88.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C4', '80.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C5', '90.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C6', '70.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C7', '75.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C8', '80.00');
INSERT INTO `saw_value` VALUES ('PS002', 'C9', '90.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C1', '80.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C10', '87.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C11', '40.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C2', '87.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C3', '86.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C4', '90.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C5', '95.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C6', '85.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C7', '89.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C8', '85.00');
INSERT INTO `saw_value` VALUES ('PS003', 'C9', '60.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C1', '87.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C10', '75.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C11', '60.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C2', '80.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C3', '98.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C4', '89.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C5', '89.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C6', '83.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C7', '85.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C8', '91.00');
INSERT INTO `saw_value` VALUES ('PS004', 'C9', '30.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C1', '90.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C10', '70.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C11', '50.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C2', '85.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C3', '88.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C5', '85.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C6', '85.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C7', '90.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C8', '86.00');
INSERT INTO `saw_value` VALUES ('PS005', 'C9', '50.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C1', '70.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C10', '82.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C11', '30.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C2', '90.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C3', '87.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C4', '75.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C5', '70.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C6', '70.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C7', '60.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C8', '95.00');
INSERT INTO `saw_value` VALUES ('PS006', 'C9', '55.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C1', '89.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C10', '76.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C11', '80.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C2', '83.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C3', '89.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C4', '91.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C5', '90.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C6', '88.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C7', '75.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C8', '50.00');
INSERT INTO `saw_value` VALUES ('PS007', 'C9', '88.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C1', '86.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C10', '65.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C11', '40.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C2', '75.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C3', '88.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C4', '87.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C5', '89.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C6', '80.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C7', '80.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C8', '60.00');
INSERT INTO `saw_value` VALUES ('PS008', 'C9', '87.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C1', '70.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C10', '86.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C11', '40.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C2', '90.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C3', '80.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C4', '95.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C5', '95.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C6', '70.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C7', '75.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C8', '86.00');
INSERT INTO `saw_value` VALUES ('PS009', 'C9', '65.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C1', '89.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C10', '65.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C11', '56.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C2', '75.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C3', '92.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C4', '94.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C5', '95.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C6', '80.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C7', '87.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C8', '40.00');
INSERT INTO `saw_value` VALUES ('PS010', 'C9', '70.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C1', '75.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C10', '100.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C11', '20.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C2', '87.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C3', '88.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C5', '65.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C6', '50.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C7', '60.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C8', '89.00');
INSERT INTO `saw_value` VALUES ('PS011', 'C9', '10.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C1', '70.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C10', '88.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C11', '60.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C2', '90.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C3', '75.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C4', '86.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C5', '87.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C6', '60.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C7', '70.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C8', '90.00');
INSERT INTO `saw_value` VALUES ('PS012', 'C9', '40.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C1', '60.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C10', '78.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C11', '50.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C2', '50.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C3', '80.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C4', '77.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C5', '86.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C6', '88.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C7', '67.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C8', '70.00');
INSERT INTO `saw_value` VALUES ('PS013', 'C9', '77.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C1', '50.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C10', '86.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C11', '30.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C2', '60.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C3', '65.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C5', '75.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C6', '65.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C7', '70.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C8', '75.00');
INSERT INTO `saw_value` VALUES ('PS014', 'C9', '80.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C1', '70.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C10', '75.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C11', '30.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C2', '75.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C3', '75.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C4', '70.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C5', '80.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C6', '60.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C7', '75.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C8', '70.00');
INSERT INTO `saw_value` VALUES ('PS015', 'C9', '65.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C1', '75.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C10', '75.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C11', '80.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C2', '85.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C3', '77.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C4', '86.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C5', '80.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C6', '76.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C7', '77.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C8', '76.00');
INSERT INTO `saw_value` VALUES ('PS016', 'C9', '60.00');
