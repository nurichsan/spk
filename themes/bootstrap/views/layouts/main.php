<!DOCTYPE html>
<html class="no-js">

    <head>
        <title>Tugas SPK</title>
        <!-- Bootstrap -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link rel="icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.ico" type="image/x-icon" />
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <style>
            table tr:first-child{
                background-color: #CC99FF;        
            }    
        </style>
    </head>

    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">SPK</a>
                    <div class="nav-collapse collapse">                        
                        <ul class="nav">
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>"><i class="icon-home"></i> Home</a>
                            </li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-bookmark"></i> Metode SAW</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo Yii::app()->createUrl('saw'); ?>"><i class="icon-refresh"></i> Proses</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('saw/person/admin'); ?>"><i class="icon-user"></i> Person</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('saw/criteria/admin'); ?>"><i class="icon-list"></i> Criteria</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-tags"></i> Alternative</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo Yii::app()->createUrl('hybrid/alternative/admin'); ?>"><i class="icon-ok"></i> Set Alternative</a></li>                                                                        
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-tags"></i> AHP</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo Yii::app()->createUrl('hybrid/default/help'); ?>"><i class="icon-question-sign"></i> Petunjuk Penggunaan</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('hybrid/criteria/admin'); ?>"><i class="icon-ok"></i> Set Criteria</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('hybrid/alternative/admin'); ?>"><i class="icon-ok"></i> Set Alternative</a></li>                                                                        
                                    <li><a href="<?php echo Yii::app()->createUrl('hybrid/criteria/pairwise'); ?>"><i class="icon-ok"></i> Pairwise</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('hybrid/electre/matrik'); ?>"><i class="icon-ok"></i> Matrik Electre</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-tags"></i> Topsis</a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo Yii::app()->createUrl('hybrid/topsis/'); ?>"><i class="icon-ok"></i> Matrik Kriteria</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('hybrid/topsis/normalisasi'); ?>"><i class="icon-ok"></i> Matrik Normalisasi</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">                

                <div class="span12" id="content">                                                                                
                    <?php echo $content; ?>        
                </div>
<!--            </div>
            <footer class="footer">                                        
                <div class="navbar navbar-fixed-bottom">                
                    <div class="navbar-inner">
                        <div class="container-fluid">     
                            <div class="nav-collapse collapse">
                                <ul class="nav pull-left">
                                    <li class="">
                                        <a href="#"> <i class="icon-bullhorn"></i> Dikembangkan sebagai tugas kuliah Sistem Pendukung Keputusan. </a>                                
                                    </li>
                                </ul>
                            </div>                        
                        </div>                
                    </div>
                </div>
            </footer>          -->
        </div>
        <!--/.fluid-container-->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendors/jquery-1.9.1.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendors/datatables/js/jquery.dataTables.min.js"></script>                
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/DT_bootstrap.js"></script>
    </body>

</html>